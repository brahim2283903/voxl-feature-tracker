/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#ifndef VOXL_FEATURE_TRACKER_H
#define VOXL_FEATURE_TRACKER_H

#include <cstddef>
#include <unordered_map>
#include <mutex>
#include <Eigen/Core>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include <modalcv.h>

#include "vft_interface.h"
#include "generic_ringbuffer.h"
#include "ringbuffer_units.h"



typedef struct feature_tracker_params {

    // some lepton stuff
    int convert_8bit_method;

    // camera params
    std::unordered_map<size_t, cv::Matx33d> cams_mats;
    std::unordered_map<size_t, cv::Vec4d> cams_dists;
    std::unordered_map<size_t, cv::Mat> cams_wrt_imu;
    std::unordered_map<size_t, bool> cams_fisheye;
    int width;
    int height;

    // tracker params
    int num_features;
    int grid_x;
    int grid_y;
    int min_pixel_dist;
    int pyramid_levels;
    int window_size;
    std::string imu_name;
    bool en_descriptors;
    bool en_gyro;
    double max_angular_rate;
    bool en_flowback;
    double flowback_pixel_max;
    bool en_refinement;
    bool en_blur;
    int blur_size;

    int lk_count;
    double lk_eps;
    double hgraphy_ransac_threshold;
    int hgraphy_max_iters;
    double hgraphy_confidence;
    int hgraphy_min_pts;

    int min_temp_delta;
    int fast_threshold;

    // general params
    bool en_debug;
    bool en_print_timing;
    bool en_timing;
    bool en_logging;
} feature_tracker_params;



class FeatureTracker {

    public:

        FeatureTracker();
        
        void init(int w, int h, int num_features_to_track, int is_lepton);

        int track_packet(const image_data &packet, std::vector<vft_feature> &tracked_feats);

        int draw_overlay(cv::Mat &overlay);

        ~FeatureTracker();

    protected:

        // main params
        feature_tracker_params params;


        int monocular_klt(const image_data &msg, size_t cam_index, std::vector<vft_feature> *feats_tracked);


        int extract_klt(std::vector<cv::Mat> &pyramid, cv::Mat &mask, std::vector<cv::Point2f> &points, std::vector<size_t> &feat_ids, size_t cam_id);


        int extract_cv_fast(cv::Mat &img, int grid_x_start, int grid_y_start, cv::Mat &mask, std::vector<cv::Point2f> &points, int num_feats_to_extract, std::vector<cv::Point2f> &old_points);


        int match_klt(std::vector<cv::Mat> &last_pyramid, std::vector<cv::Mat> &curr_pyramid, std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &new_points, size_t last_cam_id, size_t new_cam_id, std::vector<uchar> &match_mask);


        int calc_descriptors(cv::Mat &curr_img, std::vector<cv::Point2f> &curr_matched_, size_t cam_id, std::vector<vft_feature> *feats_tracked);


        int setup_imu();


        static void _new_imu_data_handler(__attribute__((unused)) int ch, char* data, int bytes, void* context);


        int get_r_over_interval(int64_t ts_prev, int64_t ts_curr, size_t cam_id, cv::Mat& R_gyro);


        int estimate_new_positions(std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &predicted_points, int64_t curr_ts, size_t cam_id);


        int estimate_new_positions(std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &predicted_points, size_t cam_id, cv::Mat &R);


        int back_project_points(std::vector<cv::Point2f> &points, size_t cam_id, std::vector<cv::Point3f> &projected_points);


        int forward_project_points(std::vector<cv::Point3f> &points, size_t cam_id, std::vector<cv::Point2f> &projected_points);


        cv::Point2f undistort_point(const cv::Point2f &cv_point, size_t cam_id, bool is_fisheye);


        float dist_between_points(const cv::Point2f &pt0, const cv::Point2f &pt1);


        template <typename T> 
        int keep_masked(std::vector<T> &v, const std::vector<uchar> &mask);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // cache
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        std::mutex cache_mutex;

        /// vector of mutexes to protect each cameras internal data
        std::vector<std::mutex> camera_mutexes;

        // last message timestamp for gyro accumulation
        int64_t last_timestamp_ns = 0;

        std::mutex oldest_lock;

        // really, we still need a cache
        std::map<size_t, std::vector<cv::Mat>> last_pyramid;

        // undistorted version of the points we just matched
        std::map<size_t, std::vector<cv::Point2f>> curr_matched_n;

        // previous location of points matched into the current frame, for overlay
        std::map<size_t, std::vector<cv::Point2f>> curr_matched;

        // previous location of points matched into the current frame, for overlay
        std::map<size_t, std::vector<cv::Point2f>> prev_matched;

        // ids corresponding to prev_matched
        std::map<size_t, std::vector<size_t>> prev_ids;

        // ids corresponding to curr_matched
        std::map<size_t, std::vector<size_t>> curr_ids;

        // last 16-bit image
        std::map<size_t, cv::Mat> last_img;


        /// atomic id for feature ids
        std::atomic<size_t> currid;

        // mcv
        #ifdef BUILD_QRB5165
        // vector of mcv features, mutex for each cvp operation
        std::mutex extractor_mutex;
        std::mutex descriptor_mutex;
        #endif

        // gyro imu ringbuffer
        RingBuffer<ImuBufferUnit> *imu_ringbuf;

        bool skipped = false;

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // timing
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        typedef struct timing_vars {
            int64_t pyr_start, pyr_end;
            int64_t extract_start, extract_end;
            int64_t pre_grid_start, pre_grid_end;
            int64_t post_grid_start, post_grid_end;
            int64_t refinement_start, refinement_end;
            int64_t estimation_start, estimation_end;
            int64_t optical_flow_start, optical_flow_end;
            int64_t ransac_start, ransac_end;
            int64_t descriptor_start, descriptor_end;
            int64_t min_pixel_start, min_pixel_end;
            int64_t total_start, total_end;
        } timing_vars;

        std::vector<timing_vars> timers;

        void reset_timer(size_t cam_id);


        // accumulated gyro over the entire interval the server has been running
        // drifty af but since we only care about small windows it should be fine
        cv::Mat total_accumulated_R;

        std::atomic<size_t> frame_id;

        vft_log_packet vft_log;

};

#endif

/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#ifndef VOXL_RINGBUF_UNITS_H
#define VOXL_RINGBUF_UNITS_H

#include <string.h> // memcpy
#include <stdint.h>
#include <modal_pipe.h>
#include <opencv2/core/mat.hpp>


#include "rc_transform.h"


////////////////////////////////////////////////////////////////////////////////
// This file defines all generics that work with our generic ringbuffer       //
// Requirements for a valid buffer unit:                                      //
// int64_t timestamp_ns - timestamp in ns                                     //
// int interpolate(Unit* A, Unit* B, double h, Unit* C)                       //
// interpolation method can be chosen by definer, but must be implemented     //
// through this wrapper                                                       //
//                                                                            //
// you can fill the buffer unit with arbitrary data regardless of size or     //
// as long as the data is NOT dynamic, i.e. must have a fixed size overall    //
////////////////////////////////////////////////////////////////////////////////


/// buffer unit with simple float data member and linear interpolation function
class FloatbufferUnit {

    public:

        FloatbufferUnit(int64_t ts, float data) : timestamp_ns(ts), d(data) {};

        FloatbufferUnit() {};


        int64_t timestamp_ns;
        float d;

        inline int interpolate(FloatbufferUnit* A, FloatbufferUnit* B, double h, FloatbufferUnit* C){
            int ret = 0;

            if(A->timestamp_ns>0 && B->timestamp_ns>0){
                int64_t diff = B->timestamp_ns - A->timestamp_ns;
                int64_t new_ts = A->timestamp_ns + (int64_t)((double)diff*h);
                C->timestamp_ns = new_ts;
            }
            else{
                C->timestamp_ns = -1;
                ret = -1;
            }

            C->d =  (double)A->d + (h*((double)B->d -(double)A->d));

            return ret;
        }
};

class ImuBufferUnit {

    public:

        ImuBufferUnit(int64_t ts, double vel[3], double ang[3]) : timestamp_ns(ts) {
            memcpy(v, vel, sizeof(v));
            memcpy(w, ang, sizeof(w));
        };

        ImuBufferUnit() {};


        int64_t timestamp_ns;

        // velocity m/s
        double v[3];

        // XYZ angular rates ('gyro values') in rad/s
        double w[3];

        inline int interpolate(ImuBufferUnit* A, ImuBufferUnit* B, double h, ImuBufferUnit* C){
            int ret = 0;
            int i = 0;

            if(A->timestamp_ns>0 && B->timestamp_ns>0){
                int64_t diff = B->timestamp_ns - A->timestamp_ns;
                int64_t new_ts = A->timestamp_ns + (int64_t)((double)diff*h);
                C->timestamp_ns = new_ts;
            }
            else{
                C->timestamp_ns = -1;
                ret = -1;
            }

            // angular velocity
            for(i=0;i<3;i++){
                C->w[i] = A->w[i] + (h*(B->w[i] - A->w[i]));
            }

            for(i=0;i<3;i++){
                C->v[i] = A->v[i] + (h*(B->v[i] - A->v[i]));
            }

            return ret;
        }
};


/// buffer unit with full transformation matrix and optional velocities
/// can perform either linear or cubic interpolation, depending on if
/// velocity fields are set
class TransformBufferUnit {

    public:

        TransformBufferUnit(int64_t ts, double data[3][4], double vel[3], double ang[3]) : timestamp_ns(ts) {
            memcpy(d, data, sizeof(d));
            memcpy(v, vel, sizeof(v));
            memcpy(w, ang, sizeof(w));
            can_do_cubic = true;
        }

        TransformBufferUnit(int64_t ts, double data[3][4]) : timestamp_ns(ts) {
            memcpy(d, data, sizeof(d));
            memset(v, 0, sizeof(v));
            memset(w, 0, sizeof(w));
            can_do_cubic = false;
        }

        TransformBufferUnit(pose_vel_6dof_t pose) {
            timestamp_ns = pose.timestamp_ns;
            for(int i=0;i<3;i++){
                d[i][3] = pose.T_child_wrt_parent[i];
                v[i] = pose.v_child_wrt_parent[i];
                w[i] = pose.w_child_wrt_child[i];
                for(int j=0;j<3;j++){
                    d[i][j] = pose.R_child_to_parent[i][j];
                }
            }
            can_do_cubic = true;
        }

        TransformBufferUnit(){}


        int64_t timestamp_ns;

        /// Rotation and translation [R|T] in row-major format.
        double d[3][4];

        // velocity m/s
        double v[3];

        // XYZ angular rates ('gyro values') in rad/s
        double w[3];

        bool can_do_cubic;

        inline int interpolate(TransformBufferUnit* A, TransformBufferUnit* B, double h, TransformBufferUnit* C){
            int ret;
            if (A->can_do_cubic && B->can_do_cubic){
                ret = cubic_interpolate(A,B,h,C);
                C->can_do_cubic = true;
            }
            else {
                ret = linear_interpolate(A,B,h,C);
                C->can_do_cubic = false;
            }

            return ret;
        }

        inline int cubic_interpolate(TransformBufferUnit* A, TransformBufferUnit* B, double h, TransformBufferUnit* C){
            int i;
            int ret = 0;

            // translation
            for(i=0;i<3;i++){
                double w1  = A->w[i];
                double w2  = B->w[i];
                double dt  = (double)(B->timestamp_ns - A->timestamp_ns) / 1000000000.0;
                double y1  = A->d[i][3];
                double y2  = B->d[i][3];
                double a   = (w1 + w2 - 2.0*((y2-y1)/dt)) / (dt*dt);
                double b   = ((w2 - w1)/(2.0*dt)) - (1.5*dt*a);
                double t   = dt*h;
                double tt  = t*t;
                double ttt = tt*t;
                C->d[i][3] = a*ttt + b*tt + w1*t + y1;
            }


            // linearly interpolate Rotation with SLERP
            // TODO try cubic here too
            double Aq[4], Bq[4], Cq[4];
            _rotation_to_quaternion(A->d, Aq);
            _rotation_to_quaternion(B->d, Bq);
            _slerp(Aq, Bq, h, Cq);
            _quaternion_to_rotation_matrix(Cq, C->d);

            // also populate output timestamp if input timestamps are valid
            if(A->timestamp_ns>0 && B->timestamp_ns>0){
                int64_t diff = B->timestamp_ns - A->timestamp_ns;
                int64_t new_ts = A->timestamp_ns + (int64_t)((double)diff*h);
                C->timestamp_ns = new_ts;
            }
            else{
                C->timestamp_ns = -1;
            }

            // angular velocity (linear)
            for(i=0;i<3;i++){
                C->w[i] = A->w[i] + (h*(B->w[i] - A->w[i]));
            }

            for(i=0;i<3;i++){
                C->v[i] = A->v[i] + (h*(B->v[i] - A->v[i]));
            }

            return 0;
        }

        inline int linear_interpolate(TransformBufferUnit* A, TransformBufferUnit* B, double h, TransformBufferUnit* C){
            int i;
            int ret = 0;

            // translation
            for(i=0;i<3;i++){
                C->d[i][3] = A->d[i][3] + (h*(B->d[i][3] - A->d[i][3]));
            }

            // linearly interpolate Rotation with SLERP
            double Aq[4], Bq[4], Cq[4];
            _rotation_to_quaternion(A->d, Aq);
            _rotation_to_quaternion(B->d, Bq);
            _slerp(Aq, Bq, h, Cq);
            _quaternion_to_rotation_matrix(Cq, C->d);

            // also populate output timestamp if input timestamps are valid
            if(A->timestamp_ns>0 && B->timestamp_ns>0){
                int64_t diff = B->timestamp_ns - A->timestamp_ns;
                int64_t new_ts = A->timestamp_ns + (int64_t)((double)diff*h);
                C->timestamp_ns = new_ts;
            }
            else{
                C->timestamp_ns = -1;
                ret = -1;
            }

            // angular velocity
            for(i=0;i<3;i++){
                C->w[i] = A->w[i] + (h*(B->w[i] - A->w[i]));
            }

            for(i=0;i<3;i++){
                C->v[i] = A->v[i] + (h*(B->v[i] - A->v[i]));
            }

            return ret;
        }
};

#endif // VOXL_RINGBUF_UNITS_H
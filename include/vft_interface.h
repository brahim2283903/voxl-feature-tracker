/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @headerfile    vft_interface.h
 * @brief         Defines types for interfacing with voxl-feature-tracker
 * 
 * @author        Thomas Patton (thomas.patton@modalai.com)
 */

#ifndef VFT_INTERFACE_H
#define VFT_INTERFACE_H

#include <stdint.h>
#include <unistd.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <modal_pipe.h>
#include <modalcv.h>

// some defines for cameras, features, and groups
#define MAX_CAMERA_GROUPS 4
#define MAX_CAMERAS_PER_GROUP 4
#define MAX_CAMERAS MAX_CAMERA_GROUPS * MAX_CAMERAS_PER_GROUP
#define MAX_OUTPUT_FEATURES MAX_CAMERAS_PER_GROUP * 100

#define LOG_CH 2
#define LOG_NAME "feat_log"
#define LOG_LOCATION MODAL_PIPE_DEFAULT_BASE_DIR LOG_NAME "/"


/// magic number for vft_feature_packet
#define VOXL_FT_MAGIC_NUMBER (0x54555249)

/// arbiitrary str buffer size for names etc
#define CHAR_BUF_SIZE 64


#ifndef DEG_TO_RAD
#define DEG_TO_RAD (M_PI/180.0)
#endif

#ifndef RAD_TO_DEG
#define RAD_TO_DEG (180.0/M_PI)
#endif



typedef enum camera_mode {
    UNKNOWN = -1,
    MONO = 0,
    STEREO = 1,
    STEREO_LEFT_ONLY = 2,
    STEREO_RIGHT_ONLY = 3
} camera_mode;


static std::string camera_mode_as_string(camera_mode cm){
    if (cm == MONO){
        return "MONO";
    }
    else if (cm == STEREO){
        return "STEREO";
    }
    else if (cm == STEREO_LEFT_ONLY){
        return "STEREO_LEFT_ONLY";
    }
    else if (cm == STEREO_RIGHT_ONLY){
        return "STEREO_RIGHT_ONLY";
    }
    else return "UNKNOWN";
}

static camera_mode string_camera_mode_to_enum(char* str_cm){
    if (!strncmp(str_cm, "MONO", sizeof("MONO"))){
        return MONO;
    }
    if (!strncmp(str_cm, "STEREO", sizeof("STEREO"))){
        return STEREO;
    }
    if (!strncmp(str_cm, "STEREO_LEFT_ONLY", sizeof("STEREO_LEFT_ONLY"))){
        return STEREO_LEFT_ONLY;
    }
    if (!strncmp(str_cm, "STEREO_RIGHT_ONLY", sizeof("STEREO_RIGHT_ONLY"))){
        return STEREO_RIGHT_ONLY;
    }
    else return UNKNOWN;
}

/**
 * @brief A type representing if the program should run the OCV tracker, the CVP
 * tracker, or both
 */
typedef enum {
    TRACKER_OCV  = 0,    // run the original OpenCV feature tracker
    TRACKER_CVP  = 1,    // run the custom CVP feature tracker
    TRACKER_BOTH = 2     // run both of the above
} tracker_type_t;

/**
 * @struct image_data
 * base packet that is fed to all of our trackers
 *
 * @field timestamp_ns      timestamp of image
 * @field tracker_ids       vec of ids per camera, matching order of images + masks
 * @field images            vec of images to track across, in order matching ids vec
 * @field masks             vec of masks to denote regions of non-interest, in order matching ids vec
 *                          mask regions with val == 255 will be ignored in tracking process
 */
typedef struct image_data {
    int64_t timestamp_ns;
    std::vector<size_t> tracker_ids;
    std::vector<cv::Mat> images;
    std::vector<cv::Mat> masks;
} image_data;


/**
 * @struct camera_info
 * internal struct used to parse out intrinsics data of cameras and store useful info
 *
 * @field name          name of the camera pipe
 * @field cam_mode      enum denoting the mode of this camera, to assist with setting up cam properties
 * @field width         image width
 * @field height        image height
 */
typedef struct camera_info {
    char name[CHAR_BUF_SIZE];
    camera_mode cam_mode;
    bool is_fisheye;
    // std::vector<cv::Matx33d> cam_mat;
    // std::vector<cv::Vec4d> dist_coeffs;
    // std::vector<Eigen::Matrix<double, 7, 1>> cam_wrt_imu;
    // std::vector<Eigen::Matrix<double, 10, 1>> cam_calib_intrinsic;
    // std::vector<cv::Mat> cam_wrt_imu_rot;

    tracker_type_t tracker_type;       // opencv, cvp, or both
} camera_info;

typedef struct tracker_input_t {
    char input_pipe[CHAR_BUF_SIZE];
    char output_pipe[CHAR_BUF_SIZE];
    char overlay_pipe[CHAR_BUF_SIZE];
    int num_features;
    tracker_type_t tracker_type;
    int group_index; // which camera group this tracker belongs to
    int camera_index; // which sub-index this camera is
} tracker_input_t;

//////////////////////////////////////////////////////////////////////////////
// EXTERNAL PACKETS                                                         //
// All structs defined below are types we are sending out over a pipe       //
//////////////////////////////////////////////////////////////////////////////


/**
 * @struct vft_feature
 * voxl-feature-tracker feature, containing all necessary info to describe a feature point
 *
 * @field id             unique id for the feature point, should correspond to a match in the previous frame
 * @fielf cam_id         unique id for the camera the feature was seen from
 * @field x              sub-pixel refined x coord of our feature
 * @field y              sub-pixel refined y coord of our feature
 * @field x_prev         sub-pixel refined previous x coord of our feature
 * @field y_prev         sub-pixel refined previous y coord of our feature
 * @field descriptor     descriptor of the feature
 * @field age            how many frames the feature has been tracked for
 * @field score          custom metric to "score" the feature
 * @field pyr_lvl_mask   mask for which pyramid levels the feature is present on
 * @field cam_id         id of the camera
 * @field reserved_1     reserved
 * @field reserved_1     reserved
 */
struct __attribute__((packed)) vft_feature {
    int64_t id;
    float x;
    float y;
    float x_prev;
    float y_prev;
    unsigned char descriptor[32] = {0};
    int32_t age;
    int8_t score;
    int8_t pyr_lvl_mask;
    int8_t cam_id;
    int8_t reserved_1;
    uint32_t reserved_2;
};


/**
 * @struct vft_feature_packet
 * voxl-feature-tracker packet, contains an entire "track update"
 * this is essentially our metadata struct
 *
 * @field magic_number      expected to be VOXL_FT_MAGIC_NUMBER
 * @field timestamp_ns      timestamp of the image extracted from
 * @field num_feats         number of vft_feature packets that will follow this message
 *
 * NOTE: 2/14/2024 removed typedef might cause issues
 */
struct __attribute__((packed)) vft_feature_packet {
    uint32_t magic_number;
    int64_t timestamp_ns;
    int32_t num_feats[4];
    int32_t frame_ids[4];
    uint8_t reset;
    uint8_t n_cams;
    uint8_t reserved_1;
    uint8_t reserved_2;

    vft_feature_packet() : n_cams(0) {}
};


typedef struct vft_log_packet{
    uint8_t packet_version;
    int64_t timestamp_ns;

    //timing variables
    int64_t time_pyr;
    int64_t time_extract;
    int64_t time_grid;
    int64_t time_grid2;
    int64_t time_refine;
    int64_t time_estimate;
    int64_t time_lk;
    int64_t time_ransac;
    int64_t time_min_pixel;
    int64_t time_descr;
    int64_t time_total;

    //homography matrix computed by findHomography
    float H00;
    float H01;
    float H02;
    float H10;
    float H11;
    float H12;
    float H20;
    float H21;
    float H22;

    uint32_t pts_last_frame;
    uint32_t pts_post_flow;
    uint32_t pts_post_flowback;
    uint32_t pts_post_ransac;
    uint32_t pts_post_min_pixel;
    uint32_t pts_extracted;

    //TODO, add more debug fields
    //number of features at various points in algorithm
    //how many features were extract by fast cv
    //something about rotation of the IMU between the frames
    //scores features we are tracking?
} __attribute((packed))__vft_log_packet;

/**
 * @brief timing helper function
 * used across mai projects
 *
 * @return int64_t monotonic time in nanoseconds
 */
static int64_t _apps_time_monotonic_ns() {
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
        fprintf(stderr, "ERROR calling clock_gettime\n");
        return -1;
    }
    return (int64_t)ts.tv_sec * 1000000000 + (int64_t)ts.tv_nsec;
}

/**
 * @brief Allocates the appropriate amount of memory for a feature packet and 
 * features
 */
static void create_vft_memory(vft_feature_packet** p_feature_packet, vft_feature** p_features) {
    *p_feature_packet = (vft_feature_packet*)malloc(sizeof(vft_feature_packet));
    *p_features = (vft_feature*)malloc(MAX_OUTPUT_FEATURES * sizeof(vft_feature));
}

/**
 * @brief Deallocates the appropriate amount of memory for a feature packet and 
 * features
 */
static void destroy_vft_memory(vft_feature_packet** p_feature_packet, vft_feature** p_features) {
    free(*p_feature_packet);
    free(*p_features);
}

/**
 * @brief Given a pipe channel and raw bytes from the pipe, parses out the 
 * feature_packet data as well as the raw feature data using the file 
 * descriptor associated with the pipe. In the case of malformed data, 
 * flushes the pipe client.
 *
 * This validation function is intended to be run inside of a simple helper
 * callback function and serves as a consistent helper to parse out the 
 * relevant feature data.
 *
 * Note: When setting up the pipe client for "tracking_feats" or another 
 * tracking pipe, it is advisable to 
 * 
 * @param ch The incoming pipe channel
 * @param data The raw data bytes from the channel
 * @param bytes The number of bytes received
 * @param feature_packet The output feature packet ptr to write to 
 * @param features The output feature pointer to write to
 */
static int validate_vft_data(int ch, char* data, int bytes, vft_feature_packet** feature_packet, vft_feature** features) {
    if(bytes != sizeof(vft_feature_packet)) {
        printf("n_bytes != feature packet, skipping...\n");
        return -1;
    }

    // check against feature packet metadata
    *feature_packet = (vft_feature_packet*)data;
    vft_feature_packet* deref_packet = *feature_packet;

    if(deref_packet->magic_number != VOXL_FT_MAGIC_NUMBER) {
        fprintf(stderr, "\nERROR: invalid metadata, magic number=%d, expected %d\n", deref_packet->magic_number, CAMERA_MAGIC_NUMBER);
        fprintf(stderr, "most likely client fell behind and pipe overflowed\n");
        pipe_client_flush(ch);
        return -1;
    }

    int n_cams = deref_packet->n_cams;
    int n_total_features = 0;
    for(int i = 0; i < n_cams; i++) {
        n_total_features += deref_packet->num_feats[i];
    }

    // tbd, may want to read into vec of vecs
    // std::vector<vft_feature> flat_features(n_total_features);
    int bytes_to_read = n_total_features * sizeof(vft_feature);

    // now read the data, this may take multiple reads as each camera 
    // is published in a list
    int total_read = 0;
    int tries = 0;
    int fd = pipe_client_get_fd(ch);
    while(tries < 10 && total_read < bytes_to_read) {
        int bytes_read = read(fd, ((char*)*features)+total_read, bytes_to_read-total_read);
        if(0 >= bytes_read) {
            printf("Error reading bytes, returning...\n");
            return -1;
        }
        // keep track of bytes read and how many tries it's taken
        total_read += bytes_read;
        tries++;
    }
    return 0;
}

#endif // VFT_INTERFACE_H

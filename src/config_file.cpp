/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#include <modal_json.h>
#include <stdio.h>
#include <voxl_common_config.h>

#include <iostream>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/stereo.hpp>
#include <string>

#include <iostream>

#include "config_file.h"

#include "rc_transform.h"

#define EXTRINS_BODY "body"


std::vector<tracker_input_t> tracker_input_vec;    // config file info about pipes to look at for tracking

char imu_name[CHAR_BUF_SIZE];

int num_features_to_track;
int grid_x;
int grid_y;
int min_pix_dist;
int pyramid_levels;
int window_size;

int width;
int height;

bool en_gyro;
bool en_descriptors;

bool en_flowback;
double flowback_pixel_max;
bool en_refinement;

bool en_blur;
int blur_size;

int lk_count;
double lk_eps;
double hgraphy_ransac_threshold;
int hgraphy_max_iters;
double hgraphy_confidence;
int hgraphy_min_pts;

bool en_logging;

int convert_8bit_method;

int fast_threshold;
int min_temp_delta;

double max_angular_rate_before_blur;


/**
 * @brief Removes old fields from the config JSON
 * 
 * @param parent Parent JSON object
 */
static void _remove_old_fields(
    cJSON* parent
) {
    // remove fields from old config file
	json_remove_if_present(parent, "cam0_enable");
	json_remove_if_present(parent, "cam0_name");
	json_remove_if_present(parent, "cam0_mode");
	json_remove_if_present(parent, "cam0_extrinsics_extension_first");
	json_remove_if_present(parent, "cam0_extrinsics_extension_second");
	json_remove_if_present(parent, "cam1_enable");
	json_remove_if_present(parent, "cam1_name");
	json_remove_if_present(parent, "cam1_mode");
	json_remove_if_present(parent, "cam1_extrinsics_extension_first");
	json_remove_if_present(parent, "cam1_extrinsics_extension_second");
	json_remove_if_present(parent, "cam2_enable");
	json_remove_if_present(parent, "cam2_name");
	json_remove_if_present(parent, "cam2_mode");
	json_remove_if_present(parent, "cam2_extrinsics_extension_first");
	json_remove_if_present(parent, "cam2_extrinsics_extension_second");
	json_remove_if_present(parent, "cam3_enable");
	json_remove_if_present(parent, "cam3_name");
	json_remove_if_present(parent, "cam3_mode");
	json_remove_if_present(parent, "cam3_extrinsics_extension_first");
	json_remove_if_present(parent, "cam3_extrinsics_extension_second");

	json_remove_if_present(parent, "imu_name");
	json_remove_if_present(parent, "num_features_to_track");
	json_remove_if_present(parent, "grid_x");
	json_remove_if_present(parent, "grid_y");
	json_remove_if_present(parent, "min_pix_dist");
	json_remove_if_present(parent, "pyramid_levels");
	json_remove_if_present(parent, "window_size");
	json_remove_if_present(parent, "en_gyro");
	json_remove_if_present(parent, "en_descriptors");
	json_remove_if_present(parent, "max_angular_rate_before_blur");
	json_remove_if_present(parent, "en_flowback");
	json_remove_if_present(parent, "flowback_pixel_max");
	json_remove_if_present(parent, "en_refinement");
	json_remove_if_present(parent, "en_blur");
	json_remove_if_present(parent, "blur_size");
	json_remove_if_present(parent, "lk_count");
	json_remove_if_present(parent, "lk_eps");
	json_remove_if_present(parent, "hgraphy_ransac_threshold");
	json_remove_if_present(parent, "hgraphy_max_iters");
	json_remove_if_present(parent, "hgraphy_confidence");
	json_remove_if_present(parent, "en_logging");
	json_remove_if_present(parent, "database_size");
	json_remove_if_present(parent, "tracker_type");
}

void make_default_groups(
    cJSON* groups_json,
    int* n_groups
) {
    // tmp vars for holding
    int int_holder;
    char string_holder[CHAR_BUF_SIZE];
    int group_counter = 0;

    // TRACKING SINGLE
    {
        // default group info
        cJSON* tracking_single = cJSON_CreateObject();
        json_fetch_bool_with_default(tracking_single, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_single, "group_name", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_single");
        json_fetch_string_with_default(tracking_single, "output_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feats");
        json_fetch_string_with_default(tracking_single, "overlay_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feat_overlay");
        cJSON* tracking_single_cams = json_fetch_array_and_add_if_missing(tracking_single, "group_cams", &int_holder);
        cJSON* tracking_cam = cJSON_CreateObject();

        // default tracking info
        json_fetch_bool_with_default(tracking_cam, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_cam, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_grey");
        json_fetch_string_with_default(tracking_cam, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_int_with_default(tracking_cam, "num_features", &int_holder, 30);

        // add tracking_cameras to group_cams
        cJSON_AddItemToArray(tracking_single_cams, tracking_cam);

        // add tracking to groups
        cJSON_AddItemToArray(groups_json, tracking_single);
        group_counter += 1;
    }

    // TRACKING LR 
    {
        // default group info
        cJSON* tracking_LR = cJSON_CreateObject();
        json_fetch_bool_with_default(tracking_LR, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_LR, "group_name", string_holder, MODAL_PIPE_MAX_PATH_LEN-1, "tracking_LR");
        json_fetch_string_with_default(tracking_LR, "output_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feats");
        json_fetch_string_with_default(tracking_LR, "overlay_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feat_overlay");
        cJSON* tracking_LR_cams = json_fetch_array_and_add_if_missing(tracking_LR, "group_cams", &int_holder);
        cJSON* tracking_L = cJSON_CreateObject();
        cJSON* tracking_R = cJSON_CreateObject();

        // default tracking info
        json_fetch_bool_with_default(tracking_L, "enable", &int_holder, 1);
        json_fetch_bool_with_default(tracking_R, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_L, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "trackingL_grey");
        json_fetch_string_with_default(tracking_R, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "trackingR_grey");
        json_fetch_string_with_default(tracking_L, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_string_with_default(tracking_R, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_int_with_default(tracking_L, "num_features", &int_holder, 30);
        json_fetch_int_with_default(tracking_R, "num_features", &int_holder, 30);

        // add cameras to group_cams
        cJSON_AddItemToArray(tracking_LR_cams, tracking_L);
        cJSON_AddItemToArray(tracking_LR_cams, tracking_R);

        // add tracking to groups
        cJSON_AddItemToArray(groups_json, tracking_LR);
        group_counter += 1;
    }

    // TRACKING FDR
    {
        // default group info
        cJSON* tracking_FDR = cJSON_CreateObject();
        json_fetch_bool_with_default(tracking_FDR, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_FDR, "group_name", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_FDR");
        json_fetch_string_with_default(tracking_FDR, "output_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feats");
        json_fetch_string_with_default(tracking_FDR, "overlay_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_feat_overlay");
        cJSON* tracking_FDR_cams = json_fetch_array_and_add_if_missing(tracking_FDR, "group_cams", &int_holder);
        cJSON* tracking_F = cJSON_CreateObject();
        cJSON* tracking_D = cJSON_CreateObject();
        cJSON* tracking_R = cJSON_CreateObject();

        // default tracking info
        json_fetch_bool_with_default(tracking_F, "enable", &int_holder, 1);
        json_fetch_bool_with_default(tracking_D, "enable", &int_holder, 1);
        json_fetch_bool_with_default(tracking_R, "enable", &int_holder, 1);
        json_fetch_string_with_default(tracking_F, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_front");
        json_fetch_string_with_default(tracking_D, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_down");
        json_fetch_string_with_default(tracking_R, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "tracking_rear");
        json_fetch_string_with_default(tracking_F, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_string_with_default(tracking_D, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_string_with_default(tracking_R, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "cvp");
        json_fetch_int_with_default(tracking_F, "num_features", &int_holder, 30);
        json_fetch_int_with_default(tracking_D, "num_features", &int_holder, 30);
        json_fetch_int_with_default(tracking_R, "num_features", &int_holder, 30);

        // add cameras to group_cams
        cJSON_AddItemToArray(tracking_FDR_cams, tracking_F);
        cJSON_AddItemToArray(tracking_FDR_cams, tracking_D);
        cJSON_AddItemToArray(tracking_FDR_cams, tracking_R);

        // add tracking to groups
        cJSON_AddItemToArray(groups_json, tracking_FDR);
        group_counter += 1;
    }

    // LEPTON
    {
        // default group info
        cJSON* lepton = cJSON_CreateObject();
        json_fetch_bool_with_default(lepton, "enable", &int_holder, 1);
        json_fetch_string_with_default(lepton, "group_name", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "lepton");
        json_fetch_string_with_default(lepton, "output_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "lepton_feats");
        json_fetch_string_with_default(lepton, "overlay_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "lepton_feat_overlay");
        cJSON* lepton_cams = json_fetch_array_and_add_if_missing(lepton, "group_cams", &int_holder);
        cJSON* lepton_cam = cJSON_CreateObject();

        // default lepton info
        json_fetch_bool_with_default(lepton_cam, "enable", &int_holder, 1);
        json_fetch_string_with_default(lepton_cam, "input_pipe", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "lepton0_raw");
        json_fetch_string_with_default(lepton_cam, "tracker_type", string_holder, 
            MODAL_PIPE_MAX_PATH_LEN-1, "ocv");    // opencv for now until we can verify cvp
        json_fetch_int_with_default(lepton_cam, "num_features", &int_holder, 30);

        // add tracking_cameras to group_cams
        cJSON_AddItemToArray(lepton_cams, lepton_cam);

        // add tracking to groups
        cJSON_AddItemToArray(groups_json, lepton);
        group_counter += 1;
    }

    // pass out the number of groups added for iter purposes
    *n_groups = group_counter;
}

int config_file_read(
) {
    // if config file does not exist, make one and initialize it with a header
    int ret = json_make_empty_file_with_header_if_missing(CONFIG_FILE, CONFIG_FILE_HEADER);
    if (ret < 0) return -1;
    if (ret > 0) fprintf(stderr, "Creating new config file: %s\n", CONFIG_FILE);
       
    // get parent JSON for reading
    cJSON *parent = json_read_file(CONFIG_FILE);
    if (parent == NULL) return -1;

    // get version of config file, if not 2.0...
    char string_holder[CHAR_BUF_SIZE];
    memset(string_holder, '\0', CHAR_BUF_SIZE);
    json_fetch_string_with_default(parent, "version", string_holder, CHAR_BUF_SIZE, "2.0");

    // now gather ptr to goups and iter though
    int n_groups;
	cJSON* groups_json = json_fetch_array_and_add_if_missing(parent, "groups", &n_groups);

    // if no camera groups, lets create some
    if(n_groups == 0) make_default_groups(groups_json, &n_groups);

    // parse groups in JSON
    for(int group_i = 0; group_i < n_groups; group_i++) {
        cJSON* group_item = cJSON_GetArrayItem(groups_json, group_i);

        // check if we actually want this camera group
        int group_item_enabled;
        json_fetch_bool_with_default(group_item, "enable", &group_item_enabled, 1);
        if(!group_item_enabled) continue;
        
        // now iter through cameras to populate data
        int n_group_cameras;
        cJSON* group_cameras_json = json_fetch_array(group_item, "group_cams", &n_group_cameras);
        for(int camera_i = 0; camera_i < n_group_cameras; camera_i++) {
            cJSON* camera_item = cJSON_GetArrayItem(group_cameras_json, camera_i);

            // check if specific cam is enabled
            int camera_item_enabled;
            json_fetch_bool_with_default(camera_item, "enable", &camera_item_enabled, 1); if(!camera_item_enabled) continue;

            tracker_input_t camera_item_input;

            // if enabled, fetch 
            json_fetch_string_with_default(camera_item, "input_pipe", camera_item_input.input_pipe, 
                MODAL_PIPE_MAX_PATH_LEN-1, "PUT_YOUR_input_pipe_HERE");
            json_fetch_string_with_default(group_item, "output_pipe", camera_item_input.output_pipe, 
                MODAL_PIPE_MAX_PATH_LEN-1, "PUT_YOUR_input_pipe_HERE");
            json_fetch_string_with_default(group_item, "overlay_pipe", camera_item_input.overlay_pipe, 
                MODAL_PIPE_MAX_PATH_LEN-1, "PUT_YOUR_input_pipe_HERE");
            json_fetch_int_with_default(camera_item, "num_features", &camera_item_input.num_features, 30);

            json_fetch_string_with_default(camera_item, "tracker_type", string_holder, 64, "cvp");
            if(!strcmp(string_holder, "cvp")) {
                camera_item_input.tracker_type = TRACKER_CVP;
            } else if(!strcmp(string_holder, "ocv")) {
                camera_item_input.tracker_type = TRACKER_OCV;
            } else if(!strcmp(string_holder, "both")) {
                camera_item_input.tracker_type = TRACKER_BOTH;
            } else {
                fprintf(stderr, "ERROR: Invalid tracker type specified in config\n");
                return -1;
            }
            camera_item_input.group_index = group_i;
            camera_item_input.camera_index = camera_i;
            tracker_input_vec.push_back(camera_item_input);
        }
    }
    // remove fields from config file v1
    _remove_old_fields(parent);

    // check if we got any errors in that process
	if(json_get_parse_error_flag()) {
		fprintf(stderr, "failed to parse data in %s\n", CONFIG_FILE);
		cJSON_Delete(parent);
		return -1;
	}
	if(json_get_modified_flag()) {
		printf("The JSON config file data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(CONFIG_FILE, parent, CONFIG_FILE_HEADER);
	}

    cJSON_Delete(parent);
    return 0;

}


/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 * @file        cvp_feature_tracker.cpp 
 * @brief       Defines functionality to invoke the CVP Feature Tracker as 
 *              defined in libmodal-cv.
 * 
 * @author      Thomas Patton (thomas.patton@modalai.com)
 */
#include <modalcv/opt_flow.h>
#include <thread>

#include <modalcv.h>

#include "cvp_feature_tracker.h"

CVPFeatureTracker::CVPFeatureTracker(){}

void CVPFeatureTracker::init(int w, int h, int is_stereo) {

    // create and populate a configuration object for our feature tracker
    mcv_cvp_feature_tracker_config_t feature_tracker_config;

    // which octave of the pyramid to perform optic flow on
    feature_tracker_config.opt_flow_octave = 2;

    // by default use entire image space (provide a mask of 1's or NULL)
    feature_tracker_config.feature_mask = NULL;

    // set up feature point extraction
    feature_tracker_config.pyr_fpx_config.width               = w;
    feature_tracker_config.pyr_fpx_config.height              = h;
    feature_tracker_config.pyr_fpx_config.n_octaves           = 3;
    feature_tracker_config.pyr_fpx_config.n_scales_per_octave = 1;
    feature_tracker_config.pyr_fpx_config.mode                = MCV_CVP_FPX_ZONE;
    feature_tracker_config.pyr_fpx_config.nms_mode            = MCV_CVP_FPX_5_TAP_NMS;
    feature_tracker_config.pyr_fpx_config.score_threshold     = 1;
    feature_tracker_config.pyr_fpx_config.robustness          = 20;                     
    feature_tracker_config.pyr_fpx_config.output_ubwc         = 1;
    feature_tracker_config.pyr_fpx_config.zone_dims[0] = 16;
    feature_tracker_config.pyr_fpx_config.zone_dims[1] = 8;
    feature_tracker_config.pyr_fpx_config.zone_dims[2] = 4;
    memset(feature_tracker_config.pyr_fpx_config.n_filter_coeffs, 
            3, MCV_CVP_MAX_PYRAMID_OCTAVES*sizeof(uint32_t));
    memset(feature_tracker_config.pyr_fpx_config.enable_fpx, 
            1, MCV_CVP_MAX_PYRAMID_OCTAVES);


    // set up opt flow
    // set up opt flow
    // w/h here are dictated by the octave we want to do flow on as defined 
    // above in the feature_tracker_config
    feature_tracker_config.opt_flow_config.width =
        w / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.height =
        h / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.en_stats = 0;
    feature_tracker_config.opt_flow_config.input_ubwc =
        1;  // pyrfpx output ubwc

    mcv_cvp_pyr_fpx_config_t pyr_fpx_config;
    mcv_cvp_opt_flow_config_t opt_flow_config;
    
    // get a handle for the feature tracker
    this->feature_tracker_handles.push_back(mcv_cvp_feature_tracker_init(
        feature_tracker_config));

    // TOOD: FIX THIS BLOCK ONCE AR144 CAMERA BUG RESOLVED
    // https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server/-/commit/1b2a787c296743ff807ecd2036225b05dff89027
    // need an extra handle for stereo    
    // if(is_stereo) {
        this->feature_tracker_handles.push_back(mcv_cvp_feature_tracker_init(
            feature_tracker_config));
    // }
}

CVPFeatureTracker::~CVPFeatureTracker(
) {
    // tear down the CVP Feature Tracker
    for(int i = 0; i < (int)this->feature_tracker_handles.size(); i++) {
        mcv_cvp_feature_tracker_deinit(this->feature_tracker_handles[i]);
    }
    return;
}

int CVPFeatureTracker::track(
    const image_data &packet,
    std::vector<vft_feature> &tracked_feats,
    int num_features_to_track
) {
    size_t img_count = packet.images.size();

    // create output structures with size according to number of images
    int16_t num_outputs[img_count];
    mcv_cvp_feature_tracker_output_t output[img_count][MAX_N_FEATURES];

    // invoke CVP feature tracker with threads if needed
    if (img_count == 1) {
        mcv_cvp_feature_tracker_process(this->feature_tracker_handles[0], 
            packet.images[0].data, &num_outputs[0], output[0]);
    } else {
        std::vector<std::thread> threads_vec;
        for (int i = 0; i < (int)img_count; i++) {
            threads_vec.push_back(std::thread(mcv_cvp_feature_tracker_process, 
                this->feature_tracker_handles[i], packet.images[i].data, &num_outputs[i], 
                output[i]));
        }

        // rejoin threads
        for (int i = 0; i < (int)img_count; i++){
            threads_vec[i].join();
        }
    }
        
    // iterate through the output features, adding them to tracked_feats
    for(int img_idx = 0; img_idx < (int)img_count; img_idx++) {
        int N = MIN(num_features_to_track, num_outputs[img_idx]); // bound number of outputs
        for(int output_i = 0; output_i < N; output_i++) {
            vft_feature feature_data;
            feature_data.id           = output[img_idx][output_i].id;
            feature_data.x            = output[img_idx][output_i].x;
            feature_data.y            = output[img_idx][output_i].y;
            feature_data.x_prev       = output[img_idx][output_i].x_prev;
            feature_data.y_prev       = output[img_idx][output_i].y_prev;
            feature_data.age          = output[img_idx][output_i].age;
            feature_data.pyr_lvl_mask = output[img_idx][output_i].pyr_lvl_mask;
            feature_data.score        = output[img_idx][output_i].score;
            feature_data.cam_id       = packet.tracker_ids[img_idx];  
            tracked_feats.push_back(feature_data);
        }
    }
    return 0;
}

int CVPFeatureTracker::draw_overlay( 
    image_data current_data,
    std::vector<vft_feature> feats_out,
    cv::Mat &overlay
) {
    int n_images = (int)current_data.images.size();
    if(n_images == 1) {
        cv::Mat current_frame = current_data.images[0];
        cv::cvtColor(current_frame, overlay, cv::COLOR_GRAY2RGB);
        for(int i = 0; i < (int)feats_out.size(); i++) {
            int age = feats_out[i].age;
            int radius, thickness;
            if(age > 15) {
                radius = 4;
                thickness = 2;
            } else {
                radius = 1;
                thickness = 1;
            }
            cv::Scalar color(std::max(0, 255-5*age), 255, 0);
            cv::Rect rect(feats_out[i].x-radius, feats_out[i].y-radius, radius*2, radius*2);
            cv::rectangle(overlay, rect, color, thickness, 1, 0);
        }
    } else { // 2 images (or more?)
        std::vector<cv::Mat> inputs;
        int offset = std::min(current_data.tracker_ids[0], current_data.tracker_ids[1]);
        for(int img_i = 0; img_i < n_images; img_i++) {
            cv::Mat current_frame = current_data.images[img_i];
            cv::cvtColor(current_frame, current_frame, cv::COLOR_GRAY2RGB);

            for(int i = 0; i < (int)feats_out.size(); i++) {
                if(feats_out[i].cam_id - offset != img_i) continue;    // pt not for this camera frame
                int age = feats_out[i].age;
                int radius, thickness;
                if(age > 15) {
                    radius = 4;
                    thickness = 2;
                } else {
                    radius = 1;
                    thickness = 1;
                }
                cv::Scalar color(std::max(0, 255-5*age), 255, 0);
                cv::Rect rect(feats_out[i].x-radius, feats_out[i].y-radius, radius*2, radius*2);
                cv::rectangle(current_frame, rect, color, thickness, 1, 0);
            }
            inputs.push_back(current_frame);
        }
        cv::vconcat(inputs, overlay);
    }
    return 0;
}

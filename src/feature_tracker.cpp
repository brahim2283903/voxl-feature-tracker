/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "feature_tracker.h"

#include <thread>
#include <opencv2/features2d.hpp>
#include <c_library_v2/common/mavlink.h>
#include <numeric>

#include <modal_journal.h>
#include <rc_math/matrix.h>
#include <rc_math/quaternion.h>
#include <rc_math/algebra.h>
#include "config_file.h"


/// Comparison functions for sorting vectors of features by score
// #ifdef BUILD_QRB5165
// static bool compare_response(mcv_fpx_feature_t first, mcv_fpx_feature_t second) { return first.score > second.score; }
// #else
static bool compare_response(cv::KeyPoint first, cv::KeyPoint second) { return first.response > second.response; }
// #endif


#define LK_ERROR_THRESHOLD 0.20
#define MSG_KLT_TIMEOUT_NS 1000000000


FeatureTracker::FeatureTracker() {}

void FeatureTracker::init(
    int w,
    int h,
    int num_features_to_track,
    int is_lepton
) {

    if(is_lepton) {
        params.width               = w;
        params.height              = h;
        params.en_debug            = 0;
        params.en_timing           = 0;
        params.en_logging          = 0;
        params.num_features        = 100; // override?
        params.grid_x              = 1;
        params.grid_y              = 1;
        params.min_pixel_dist      = 8;
        params.pyramid_levels      = 2;
        params.window_size         = 8;
        params.imu_name            = "imu_apps";
        params.en_gyro             = false;
        params.en_flowback         = true;
        params.flowback_pixel_max  = 1.5;
        params.en_refinement       = true;
        params.en_blur             = true;
        params.blur_size           = 3;
        params.lk_count            = 30;
        params.lk_eps              = 0.01;
        params.max_angular_rate    = 40;
        params.convert_8bit_method = 1;
    } else {
        params.width               = w;
        params.height              = h;
        params.en_debug            = 0;
        params.en_timing           = 0;
        params.en_logging          = 0;
        params.num_features        = num_features_to_track;
        params.grid_x              = 1;
        params.grid_y              = 1;
        params.min_pixel_dist      = 5;
        params.pyramid_levels      = 2;
        params.window_size         = 8;
        params.imu_name            = "imu_apps";
        params.en_gyro             = false;
        params.en_flowback         = true;
        params.flowback_pixel_max  = 1;
        params.en_refinement       = true;
        params.en_blur             = true;
        params.blur_size           = 3;
        params.lk_count            = 30;
        params.lk_eps              = 0.01;
        params.max_angular_rate    = 40;
        params.convert_8bit_method = 1;
    }
    // set up internal vectors
    timers.resize(5);

    if (camera_mutexes.empty() || camera_mutexes.size() != params.cams_mats.size()) {
        std::vector<std::mutex> mut_list(MAX_CAMERAS);    
        camera_mutexes.swap(mut_list);
    }

    currid = 1025;
    frame_id = 0;
}


int FeatureTracker::track_packet(const image_data &packet, std::vector<vft_feature> &tracked_feats){
    // safety checks
    if (packet.tracker_ids.empty() || packet.tracker_ids.size() != packet.images.size() ||packet.images.size() != packet.masks.size()){
        M_ERROR("malformed data packet received in %s\n", __FUNCTION__);
        return -1;
    }

    if (params.en_logging) {
        memset(&vft_log, 0, sizeof(vft_log));
        vft_log.packet_version = 1;
        vft_log.timestamp_ns = packet.timestamp_ns;
    }

    // accumulate our gyro over new interval before we start tracking and enter possible failure cases
    // TODO we also need to keep track of timestamps per camera so we accumulate over different rates, probably need a bigger buffer just in case
    // cv::Mat last_frame_accum = cv::Mat::eye(3, 3, CV_64F);
    // get_r_over_interval(last_timestamp_ns, packet.timestamp_ns, 0, last_frame_accum);

    // if (total_accumulated_R.empty()){
    //     total_accumulated_R = last_frame_accum;
    // }
    // else total_accumulated_R *= last_frame_accum;
    // end temporary gyro code

    size_t img_count = packet.images.size();
    int ret = 0;

    if (img_count == 1){
        ret = monocular_klt(packet, 0, &tracked_feats);
    }
    else {
        std::vector<std::thread> threads_vec;

        for (int i = 0; i < (int)img_count; i++){
            threads_vec.push_back(std::thread(&FeatureTracker::monocular_klt,this,packet,i,&tracked_feats));
        }

        // loop back through and join em up
        for (int i = 0; i < (int)img_count; i++){
            threads_vec[i].join();
        }
    }


    // TODO: redo timing helpers into a basic class
    // timing stats
    if (params.en_timing){
        int64_t final_time = 0;
        for (int i = 0; i < (int)img_count; i++){
            int64_t final_estimate = 0;
            int64_t final_descr = 0;
            int64_t final_pyr = (timers[packet.tracker_ids[i]].pyr_end - timers[packet.tracker_ids[i]].pyr_start);
            int64_t final_extract = (timers[packet.tracker_ids[i]].extract_end - timers[packet.tracker_ids[i]].extract_start);
            int64_t final_grid = (timers[packet.tracker_ids[i]].pre_grid_end -timers[packet.tracker_ids[i]].pre_grid_start);
            int64_t final_grid2 = (timers[packet.tracker_ids[i]].post_grid_end - timers[packet.tracker_ids[i]].post_grid_start);
            int64_t final_refine = (timers[packet.tracker_ids[i]].refinement_end - timers[packet.tracker_ids[i]].refinement_start);
            if (params.en_gyro) final_estimate = (timers[packet.tracker_ids[i]].estimation_end - timers[packet.tracker_ids[i]].estimation_start);
            int64_t final_lk = (timers[packet.tracker_ids[i]].optical_flow_end - timers[packet.tracker_ids[i]].optical_flow_start);
            int64_t final_ransac = (timers[packet.tracker_ids[i]].ransac_end - timers[packet.tracker_ids[i]].ransac_start);
            int64_t final_min_pixel = (timers[packet.tracker_ids[i]].min_pixel_end - timers[packet.tracker_ids[i]].min_pixel_start);
            if (params.en_descriptors) final_descr = (timers[packet.tracker_ids[i]].descriptor_end - timers[packet.tracker_ids[i]].descriptor_start);
            int64_t total_time = std::max(timers[packet.tracker_ids[i]].total_end - timers[packet.tracker_ids[i]].total_start, (int64_t)0);

            if (params.en_logging) {
                vft_log.time_pyr = final_pyr;
                vft_log.time_extract = final_extract;
                vft_log.time_grid = final_grid;
                vft_log.time_grid2 = final_grid2;
                vft_log.time_refine = final_refine;
                vft_log.time_estimate = final_estimate;
                vft_log.time_lk = final_lk;
                vft_log.time_ransac = final_ransac;
                vft_log.time_min_pixel = final_min_pixel;
                vft_log.time_descr = final_descr;
                vft_log.time_total = total_time;
            }
            if (params.en_timing) {
                fprintf(stderr, "========================CAM %d===============================================\n", i);
                fprintf(stderr, "Pyr Extraction:             %6.5fms\n", (double)final_pyr/1000000.);
                fprintf(stderr, "Feat Extraction:            %6.5fms\n", (double)final_extract/1000000.);
                fprintf(stderr, "Pre-Grid:                   %6.5fms\n", (double)final_grid/1000000.);
                fprintf(stderr, "Post-Grid:                  %6.5fms\n", (double)final_grid2/1000000.);
                fprintf(stderr, "Refinement:                 %6.5fms\n", (double)final_refine/1000000.);
                if (params.en_gyro) fprintf(stderr, "Location Estimation: %6.5fms\n", (double)final_estimate/1000000.);
                fprintf(stderr, "LK Optical Flow:            %6.5fms\n", (double)final_lk/1000000.);
                fprintf(stderr, "Ransac:                     %6.5fms\n", (double)final_ransac/1000000.);
                if (params.en_descriptors) fprintf(stderr, "Descriptor Calc took: %6.5fms\n", (double)final_descr/1000000.);
                fprintf(stderr, "Score feature elimination:  %6.5fms\n", (double)final_min_pixel/1000000.);
                fprintf(stderr, "CAM TOTAL TIME:             %6.5fms\n", (double)total_time/1000000.);
                fprintf(stderr, "============================================================================\n\n");
            }

            reset_timer(packet.tracker_ids[i]);

            final_time += total_time;
        }
        if(params.en_timing) {
            fprintf(stderr, "============================================================================\n");
            fprintf(stderr, "FINAL TIME: %6.5fms\n", (double)final_time/1000000.);
            fprintf(stderr, "============================================================================\n\n");
        }
    }

    //publish log data for this track
    if (params.en_logging) {
        pipe_server_write(LOG_CH, &vft_log, sizeof(vft_log_packet));
    }

    frame_id++;

    // TODO: need a way to return error codes back to here
    return ret;
}


int FeatureTracker::monocular_klt(
    const image_data &msg,
    size_t cam_index,
    std::vector<vft_feature> *feats_tracked
){
    size_t id = msg.tracker_ids[cam_index];

    std::lock_guard<std::mutex> outer_guard(camera_mutexes[cam_index]);
    if (params.en_timing) timers[id].total_start = _apps_time_monotonic_ns();

    cv::Mat img, mask;
    img = msg.images[cam_index];
    mask = msg.masks[cam_index];

    //blur before converting from 16-bit to 8-bit
    if (params.en_blur) {
        cv::blur(img, img, cv::Size(params.blur_size, params.blur_size));
    }

    cv::Mat img_post_blur = img.clone();

    cv::Mat last_img8;
    if(img.type() == CV_16UC1) {
        //need to convert 16 bit image to 8 bit
        double min_pixel, max_pixel;
        cv::minMaxLoc(img, &min_pixel, &max_pixel);

        if (params.convert_8bit_method == 1 && !last_img[id].empty()) {
            //also consider the previous image when determining the conversion
            double last_min_pixel, last_max_pixel;
            cv::minMaxLoc(last_img[id], &last_min_pixel, &last_max_pixel);

            max_pixel = std::max(max_pixel, last_max_pixel);
            min_pixel = std::min(min_pixel, last_min_pixel);
        }        

        double pixel_diff = std::max(max_pixel - min_pixel, (double)params.min_temp_delta);

        double alpha_convert = 255.0/pixel_diff;
        double beta_convert = -min_pixel*alpha_convert;

        cv::Mat img_8;
        img.convertTo(img_8, CV_8UC1, alpha_convert, beta_convert);
        img = img_8;

        if (params.convert_8bit_method == 1 && !last_img[id].empty()) {
            last_img[id].convertTo(last_img8, CV_8UC1, alpha_convert, beta_convert);
        }
    }

    if (params.en_timing) timers[cam_index].pyr_start = _apps_time_monotonic_ns();

    std::vector<cv::Mat> imgpyr;
    cv::buildOpticalFlowPyramid(img, imgpyr, cv::Size(params.window_size, params.window_size), params.pyramid_levels, false);

    if (params.en_timing) timers[cam_index].pyr_end = _apps_time_monotonic_ns();

    //printf("frame_id, n_pts last: %d, %d\n", (int)frame_id, (int)curr_matched[cam_index].size());

    // first frame
    if (!frame_id || curr_matched[cam_index].size()==0) {
        std::vector<cv::Point2f> new_points;
        std::vector<size_t> new_ids;
        extract_klt(imgpyr, mask, new_points, new_ids, id);

        std::lock_guard<std::mutex> prev_lock(cache_mutex);

        last_timestamp_ns = msg.timestamp_ns;

        for (size_t i = 0; i < new_points.size(); i++){
            vft_feature feat;
            feat.id = new_ids[i];
            feat.cam_id = cam_index;
            feat.x = new_points[i].x;
            feat.y = new_points[i].y;
            feats_tracked->push_back(feat);
        }

        // cache data, undistorted points are only set after a match call
        last_pyramid[cam_index] = imgpyr;
        prev_matched[cam_index].clear();
        curr_matched[cam_index] = new_points;
        prev_ids[cam_index].clear();
        curr_ids[cam_index] = new_ids;
        last_img[cam_index] = img_post_blur;

        // signal no matches
        return -1;
    }
    // outer containers for combined measurements 
    std::vector<cv::Point2f> final_points;
    std::vector<uchar> matches_mask;

    std::vector<cv::Point2f> last_points = curr_matched[cam_index];
    std::vector<size_t> last_ids = curr_ids[cam_index];

    //////////////////////////////////////////////////////////////////////////////////////
    // check the angle around the exposure period, then check angular rate using
    // that angle and the full dt from last frame
    // this is to detect motion blur
    //////////////////////////////////////////////////////////////////////////////////////
    int64_t last_timestamp_cp = last_timestamp_ns;
    // cv::Mat accumulated_R = cv::Mat::eye(4, 4, CV_64F);

    int ret = 0;

    // only check this if the user set it something positive, otherwise ignore
    // if (params.max_angular_rate > 0){
    //     get_r_over_interval(msg.timestamp_ns-30e6, msg.timestamp_ns+30e6, id, accumulated_R);

    //     // now convert to axis angle and check if its within a threshold
    //     rc_matrix_t curr_R_diff = RC_MATRIX_INITIALIZER;
    //     rc_matrix_identity(&curr_R_diff, 3);

    //     // fill rc_matrix
    //     for(int i=0;i<3;i++){
    //         for(int j=0;j<3;j++){
    //             curr_R_diff.d[i][j] = accumulated_R.at<double>(i, j);
    //         }
    //     }

    //     // convert to axis angle
    //     rc_vector_t axis = RC_VECTOR_INITIALIZER;
    //     double angle = 0;
    //     ret = rc_rotation_matrix_to_axis_angle(curr_R_diff, &axis, &angle);

    //     // clean memory
    //     rc_matrix_free(&curr_R_diff);
    //     rc_vector_free(&axis);

    //     // failure to convert to axis angle
    //     if (ret){
    //         // assume angle is okay
    //         if (params.en_debug)  M_ERROR("Failed to convert to axis angle\n");
    //         angle = 0;
    //     }
    //     else {
    //         angle *= RAD_TO_DEG;
    //         angle /= (double)((msg.timestamp_ns - last_timestamp_cp)/1e9);
    //     }

    //     if (angle > params.max_angular_rate){
    //         skipped = true;
    //         M_WARN("SKIPPING frame with high angle\n");
    //         return -2;
    //     }
    // }

    std::vector<cv::Point2f> points_now;
    std::vector<size_t> ids_now;


    if (params.en_debug) printf("last frame number of points: %d\n", (int)last_points.size());
    if (params.en_logging) vft_log.pts_last_frame = last_points.size();

    // Steps
    // Try to match the points from the last frame to this frame using calcOpticalFlowPyrLK
    // Check validity of points with flowback check
    // Run Homography Check - output of this algorithm is the flow!
    // Extract features from this frame for the next iteration
    
    std::vector<cv::Point2f> matched_pts_flow;
    std::vector<uchar> matches_mask_flow;

    std::vector<cv::Mat> last_imgpyr = last_pyramid[cam_index];
    if (params.convert_8bit_method == 1 && !last_img8.empty()) {
        // must regenerate last image pyramid from last image with new rescaling
        cv::buildOpticalFlowPyramid(last_img8, last_imgpyr, cv::Size(params.window_size, params.window_size), params.pyramid_levels, false);
    }

    int ret_match_klt = match_klt(last_imgpyr, imgpyr, last_points, matched_pts_flow, cam_index, cam_index, matches_mask_flow);

    // matched_pts_flow should be same size as last_points
    // matches_mask_flow indicates whether a particular id is a good match
    // the flow computation should be done at this point

    // then extract new features to fill out the rest of the image
    // get the features and ids to pass to extract


    for (size_t i = 0; i < matched_pts_flow.size(); i++) {
        if (matches_mask_flow[i])
        {
            points_now.push_back(matched_pts_flow[i]);
            ids_now.push_back(last_ids[i]);
        }
    }


    int ret_extract_klt = extract_klt(imgpyr, mask, points_now, ids_now, cam_index); //TODO: check difference between id and cam_index

    final_points = points_now;

    curr_matched_n[cam_index].clear();

    for (size_t i = 0; i < final_points.size(); i++)
    {
        matches_mask.push_back((uchar)1);
        // curr_matched_n[cam_index].push_back(undistort_point(final_points.at(i), cam_index, params.cams_fisheye.at(cam_index)));
        curr_matched_n[cam_index].push_back(final_points.at(i));   
    }

    skipped = false;

    std::vector<cv::Point2f> curr_match;
    std::vector<cv::Point2f> prev_match;
    std::vector<size_t> ids;

    for (size_t i = 0; i < final_points.size(); i++) {
        // another double check
        if (final_points[i].x < 0 || final_points[i].y < 0 || (int)final_points[i].x >= img.cols || (int)final_points[i].y >= img.rows)
            continue;

        if (matches_mask[i]) {
            //this feels dangerous as we thread this function
            // TODO: make each cam have their own vec that gets
            // concatenated by the outer thread manager at the top
            // if ((int)feats_tracked->size() < params.num_features){
            vft_feature feat;
            feat.id = ids_now[i];
            feat.cam_id = cam_index;
            feat.x = final_points[i].x;
            feat.y = final_points[i].y;
            feat.x_prev = curr_matched_n[cam_index][i].x;
            feat.y_prev = curr_matched_n[cam_index][i].y;
            feats_tracked->push_back(feat);
            // }

            curr_match.push_back(final_points[i]);
            ids.push_back(ids_now[i]);
        }
    }

    // TODO: this needs to just take in the feats_tracked vector and modify it
    // doesn't need the curr_match vector
    // if (params.en_descriptors && !feats_tracked->empty()){
    //     ret = calc_descriptors(img, curr_match, id, feats_tracked);
    // }

    // cache lock
    std::lock_guard<std::mutex> prev_lock(cache_mutex);

    last_timestamp_ns = msg.timestamp_ns;
    last_pyramid[cam_index] = imgpyr;

    prev_ids[cam_index] = last_ids;
    prev_matched[cam_index] = last_points;

    curr_ids[cam_index] = ids;
    curr_matched[cam_index] = curr_match;

    last_img[id] = img_post_blur;

    if (params.en_timing) timers[id].total_end = _apps_time_monotonic_ns();

    return 0;
}


int FeatureTracker::extract_klt(
    std::vector<cv::Mat> &pyramid,
    cv::Mat &mask,
    std::vector<cv::Point2f> &points,
    std::vector<size_t> &feat_ids,
    size_t cam_id
) {
    if (pyramid.empty()){
        M_ERROR("%s RECEIVED EMPTY PYRAMID\n", __FUNCTION__);
        return -1;
    }

    if (params.en_timing) timers[cam_id].pre_grid_start = _apps_time_monotonic_ns();
    int num_features_grid = (int)((double)params.num_features / (double)(params.grid_x * params.grid_y));
    if (!num_features_grid) num_features_grid = 1;

    int grid_x_div_factor = (float)pyramid.at(0).cols / params.grid_x;
    int grid_y_div_factor = (float)pyramid.at(0).rows / params.grid_y;

    cv::Size grid_size(params.grid_x, params.grid_y);

    // TODO: double check this logic
    cv::Size pix_dist_size((int)((float)pyramid.at(0).cols / (float)params.min_pixel_dist), (int)((float)pyramid.at(0).rows / (float)params.min_pixel_dist));

    cv::Mat grid = cv::Mat(grid_size, CV_8UC1, cv::Scalar(num_features_grid));
    cv::Mat pix_dist_grid = cv::Mat::zeros(pix_dist_size, CV_8UC1);

    auto it0 = points.begin();
    auto it1 = feat_ids.begin();
    while (it0 != points.end()) {
        cv::Point2f kpt = *it0;
        int x = (int)kpt.x;
        int y = (int)kpt.y;

        int x_pix_dist = (int)(kpt.x / (float)params.min_pixel_dist);
        int y_pix_dist = (int)(kpt.y / (float)params.min_pixel_dist);
        // this is just a bounds check, need to make sure that we dont go out of bounds
        if (x_pix_dist < 0 || x_pix_dist >= pix_dist_size.width || y_pix_dist < 0 || y_pix_dist >= pix_dist_size.height) {
            it0 = points.erase(it0);
            it1 = feat_ids.erase(it1);
            continue;
        }

        int x_grid = std::floor(kpt.x / grid_x_div_factor);
        int y_grid = std::floor(kpt.y / grid_y_div_factor);
        if (x_grid < 0 || x_grid >= params.grid_x || y_grid < 0 || y_grid >= params.grid_y) {
            it0 = points.erase(it0);
            it1 = feat_ids.erase(it1);
            continue;
        }

        // if there is already a point here, delete this one
        if (pix_dist_grid.at<uint8_t>(y_pix_dist, x_pix_dist) > 0) {
            it0 = points.erase(it0);
            it1 = feat_ids.erase(it1);
            continue;
        }
        else {
            // otherwise indicate there is a point here
            pix_dist_grid.at<uint8_t>(y_pix_dist, x_pix_dist) += 1;
        }
        // check if masked region, delete if so
        if (mask.at<uint8_t>(y, x) == 255) {
            it0 = points.erase(it0);
            it1 = feat_ids.erase(it1);
            continue;
        }
        // increment grids to denote how many features per grid we need
        if (grid.at<uint8_t>(y_grid, x_grid) > 0) {
            grid.at<uint8_t>(y_grid, x_grid) -= 1;
        }

        it0++;
        it1++;
    }

    if (params.en_timing) timers[cam_id].pre_grid_end = _apps_time_monotonic_ns();

    // // if we need < 20% of our total features, just ignore
    // if (params.num_features - (int)points.size() < (0.2 * params.num_features)){
    //     return -1;
    // }

    cv::Mat grid_mask;
    cv::resize(mask, grid_mask, grid_size, 0.0, 0.0, cv::INTER_NEAREST);

    std::vector<cv::KeyPoint> kpts0_new;
    std::vector<cv::Point2f> pts0_new;

    if (params.en_timing) timers[cam_id].extract_start = _apps_time_monotonic_ns();

    // do fast extraction over the grid that we need features from
    // this will need to be parallelizable, so another function

    // eventually try doing fast extraction over the whole image and then split up results
    // more thread magic
    std::vector<std::thread> threads_vec;
    std::vector<std::vector<cv::Point2f>> grid_points_vec(params.grid_x * params.grid_y);
    int grid_idx_x = 0;
    int grid_idx_y = 0;

    for (int i = 0; i < params.grid_x * params.grid_y; i++){
        if (grid.at<uint8_t>(grid_idx_y, grid_idx_x) > 1) threads_vec.push_back(std::thread(&FeatureTracker::extract_cv_fast, this, std::ref(pyramid[0]), grid_idx_x, grid_idx_y, std::ref(mask), std::ref(grid_points_vec[grid_idx_x + (grid_idx_y * params.grid_x)]), (int)grid.at<uint8_t>(grid_idx_y, grid_idx_x), std::ref(points)));
        if (grid_idx_x == params.grid_x - 1){
            grid_idx_x = 0;
            grid_idx_y += 1;
        }
        else grid_idx_x++;
    }

    // loop back through and join em up
    for (size_t i = 0; i < threads_vec.size(); i++){
        threads_vec[i].join();
    }

    if (params.en_timing) timers[cam_id].extract_end = _apps_time_monotonic_ns();
    if (params.en_timing) timers[cam_id].post_grid_start = _apps_time_monotonic_ns();

    // loop back through the vector again, skipping empty indices
    // then fill in the points we just extracted and get ready for refinement
    for (int i = 0; i < params.grid_x * params.grid_y; i++){
        if (grid_points_vec[i].empty()) continue;
        else{
            for (size_t j = 0; j < grid_points_vec[i].size(); j++){
                pts0_new.push_back(grid_points_vec[i][j]);
            }
        }
    }
    
    if (params.en_debug) printf("new points extracted: %d\n", (int)pts0_new.size());
    if (params.en_logging) vft_log.pts_extracted = pts0_new.size();

    if (params.en_timing) timers[cam_id].post_grid_end = _apps_time_monotonic_ns();


    if (params.en_timing) timers[cam_id].refinement_start = _apps_time_monotonic_ns();

    if (!pts0_new.empty()){
        if (params.en_refinement) {
            cv::Size win_size = cv::Size(3, 3);
            cv::Size zero_zone = cv::Size(-1, -1);
            cv::TermCriteria term_crit = cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 15, 0.01);
            cv::cornerSubPix(pyramid.at(0), pts0_new, win_size, zero_zone, term_crit);
        }

        for (size_t i = 0; i < pts0_new.size(); i++) {
            points.push_back(pts0_new.at(i));

            size_t temp = ++currid;
            feat_ids.push_back(temp);
        }
    }

    if (params.en_timing) timers[cam_id].refinement_end = _apps_time_monotonic_ns();

    return 0;
}



int FeatureTracker::extract_cv_fast(cv::Mat &img, int grid_x_start, int grid_y_start, cv::Mat &mask, std::vector<cv::Point2f> &points, int num_feats_to_extract, std::vector<cv::Point2f> &old_points){
    if (params.en_debug) printf("extract_cv_fast num_feats_to_extract: %d\n", (int)num_feats_to_extract);

    if (num_feats_to_extract <= 0) return -1;

    std::vector<cv::Point2f> all_points = old_points;

    // basically, we are just going to pass a single grid to this function and extract from it
    // thus, it can be parallelized by just looping through each grid that needs points
    int x_side = img.cols/params.grid_x;
    int y_side = img.rows/params.grid_y;

    int grid_coord_x = grid_x_start * x_side;
    int grid_coord_y = grid_y_start * y_side;

    cv::Rect img_grid = cv::Rect(grid_coord_x, grid_coord_y, x_side, y_side);
    std::vector<cv::KeyPoint> pts_new;
    cv::FAST(img(img_grid), pts_new, params.fast_threshold, true);
    
    std::sort(pts_new.begin(), pts_new.end(), compare_response);

    //account for min_pixel_distance when adding points
    int pts_added = 0;
    for (size_t i = 0; i < pts_new.size(); i++) {
        //printf("pt, response: %d, %f\n", (int)i, pts_new.at(i).response);

        // create point
        cv::Point2f curr_pt = cv::Point2f(pts_new.at(i).pt.x, pts_new.at(i).pt.y);
        curr_pt.x += (float)grid_coord_x;
        curr_pt.y += (float)grid_coord_y;

        // check for out of bounds (shouldn't be possible...)
        if ((int)curr_pt.x < 0 || (int)curr_pt.x > img.cols || (int)curr_pt.y < 0 ||
            (int)curr_pt.y > img.rows)
            continue;

        // NOTE: mask has max value of 255 (white) if it should be removed
        if (mask.at<uint8_t>((int)curr_pt.y, (int)curr_pt.x) == 255)
            continue;

        // check the distance to all other points
        bool add_point = true;
        for (const auto& pt : all_points) {
            if (dist_between_points(curr_pt, pt) < params.min_pixel_dist) {
                add_point = false;
                break;
            }
        }


        if (add_point) {
            pts_added += 1;
            points.push_back(curr_pt);
            all_points.push_back(curr_pt);

            if (pts_added == num_feats_to_extract) return 0;
        }
    }

    return 0;
}

int FeatureTracker::match_klt(std::vector<cv::Mat> &last_pyramid, std::vector<cv::Mat> &curr_pyramid, std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &new_points, size_t last_cam_id, size_t new_cam_id, std::vector<uchar> &match_mask){
    if (last_points.empty()){
        return -1;
    }

    for (size_t i = 0; i < last_points.size(); i++){
        match_mask.push_back((uchar)0);
    }

    static cv::Size window_size = cv::Size(params.window_size, params.window_size);

    std::vector<uchar> mask_klt;
    std::vector<float> error;
    std::vector<cv::Point2f> pts0 = last_points;
    std::vector<cv::Point2f> pts1;

    if (params.en_timing) timers[new_cam_id].optical_flow_start = _apps_time_monotonic_ns();


    cv::TermCriteria term_crit = cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, params.lk_count, params.lk_eps);

    // lk
    // cv::calcOpticalFlowPyrLK(last_pyramid, curr_pyramid, pts0, pts1, mask_klt, error, window_size, params.pyramid_levels, term_crit, cv::OPTFLOW_USE_INITIAL_FLOW | cv::OPTFLOW_LK_GET_MIN_EIGENVALS);
    
    // don't use initial flow
    cv::calcOpticalFlowPyrLK(last_pyramid, curr_pyramid, pts0, pts1, mask_klt, error, window_size, params.pyramid_levels, term_crit, cv::OPTFLOW_LK_GET_MIN_EIGENVALS);

    //get just the points that passed
    std::vector<cv::Point2f> pts0_now, pts1_now;
    std::vector<size_t> ids_now;

    for (size_t i = 0; i < pts0.size(); i++) {
        if(mask_klt[i]) {
            ids_now.push_back(i);
            pts0_now.push_back(pts0[i]);
            pts1_now.push_back(pts1[i]);
        }
    }
    
    if (params.en_debug) printf("n pts post flow: %d\n", (int)pts0_now.size());
    if (params.en_logging) vft_log.pts_post_flow = pts0_now.size();

    if (params.en_timing) timers[new_cam_id].optical_flow_end = _apps_time_monotonic_ns();
    if (pts0_now.size()==0) return -3;

    if (params.en_flowback) {
        //check the flow in the reverse direction
        std::vector<uchar> mask_klt_rev;
        std::vector<float> error_rev;
        std::vector<cv::Point2f> pts0_now_rev;

        cv::calcOpticalFlowPyrLK(curr_pyramid, last_pyramid, pts1_now, pts0_now_rev, mask_klt_rev, error_rev, window_size, params.pyramid_levels, term_crit, cv::OPTFLOW_LK_GET_MIN_EIGENVALS);

        for (size_t i = 0; i < pts0_now_rev.size(); i++) {
            if (mask_klt_rev[i]) {
                //check the distance between the original point and flowback point
                if (dist_between_points(pts0_now[i], pts0_now_rev[i]) > params.flowback_pixel_max) {
                    //printf("failed flowback, i, dist: %d, %f\n", (int)i, dist);
                    mask_klt_rev[i] = (uchar)0;
                }
            }
        }

        //only keep points that passed flowback check
        keep_masked(ids_now, mask_klt_rev);
        keep_masked(pts0_now, mask_klt_rev);
        keep_masked(pts1_now, mask_klt_rev);

        if (params.en_debug) printf("n pts post flowback: %d\n", (int)pts0_now.size());
        if (params.en_timing) timers[new_cam_id].optical_flow_end = _apps_time_monotonic_ns();
        if (pts0_now.size()==0) return -4;
    }

    if (params.en_logging) vft_log.pts_post_flowback = pts0_now.size();

    if (params.en_timing) timers[new_cam_id].ransac_start = _apps_time_monotonic_ns();

    // // undistort for ransac
    // if (pts0_now.size() < 6) { //TODO, make the number of points used configurable
    //     if (params.en_debug) printf("need at least 6 points for findHomography: %d\n", (int)pts0_now.size());
    //     if (params.en_timing) timers[new_cam_id].ransac_end = _apps_time_monotonic_ns();
    //     return -2;
    // }

    // for (size_t i = 0; i < pts0_now.size(); i++) {
    //     pts0_n.push_back(undistort_point(pts0_now.at(i), last_cam_id, params.cams_fisheye.at(last_cam_id)));
    //     pts1_n.push_back(undistort_point(pts1_now.at(i), new_cam_id, params.cams_fisheye.at(new_cam_id)));
    // }

    std::vector<cv::Point2f> pts0_n, pts1_n;
    pts0_n = pts0_now;
    pts1_n = pts1_now;

    // RANSAC
    // std::vector<uchar> mask_rsc;

    // // Mat cv::findHomography(InputArray srcPoints,InputArray dstPoints, int method = 0, double ransacReprojThreshold = 3, OutputArray 	mask = noArray(), const int maxIters = 2000,const double confidence = 0.995)
    // //this calculation is done is normalized camera coordinate space so the error threshold must be much lower than for pixels
    // cv::Mat H_out = cv::findHomography(pts0_n, pts1_n, cv::RANSAC, params.hgraphy_ransac_threshold, mask_rsc, params.hgraphy_max_iters, params.hgraphy_confidence);

    // if (params.en_logging) {
    //     if (H_out.rows == 3 && H_out.cols == 3) {
    //         vft_log.H00 = H_out.at<double>(0,0);
    //         vft_log.H01 = H_out.at<double>(0,1);
    //         vft_log.H02 = H_out.at<double>(0,2);
    //         vft_log.H10 = H_out.at<double>(1,0);
    //         vft_log.H11 = H_out.at<double>(1,1);
    //         vft_log.H12 = H_out.at<double>(1,2);
    //         vft_log.H20 = H_out.at<double>(2,0);
    //         vft_log.H21 = H_out.at<double>(2,1);
    //         vft_log.H22 = H_out.at<double>(2,2);
    //     }
    // }

    // //only keep points that passed RANSAC
    // keep_masked(ids_now, mask_rsc);
    // keep_masked(pts0_now, mask_rsc);
    // keep_masked(pts1_now, mask_rsc);

    // if (params.en_debug) printf("n pts post ransac: %d\n", (int)pts0_now.size());
    // if (params.en_logging) vft_log.pts_post_ransac = pts0_now.size();
    
    
    if (params.en_timing) timers[new_cam_id].ransac_end = _apps_time_monotonic_ns();
    if(pts0_now.size()==0) return -5;

    // git rid of points that are too close
    if (params.en_timing) timers[new_cam_id].min_pixel_start = _apps_time_monotonic_ns();

    std::vector<uchar> mask_min_pixel(pts0_now.size());

    //first sort the error vector, higher scores are better
    std::vector<float> error_now;
    for (size_t i = 0; i < ids_now.size(); i++) {
        error_now.push_back(error[ids_now[i]]);
    }

    std::vector<size_t> idx(error_now.size());
    std::iota(idx.begin(), idx.end(), 0);
    std::stable_sort(idx.begin(), idx.end(), [&error_now](size_t i1, size_t i2) {return error_now[i1] > error_now[i2];});

    std::vector<cv::Point2f> output_pts;

    //go through the idx list, highest scores first
    for (size_t i = 0; i < idx.size(); i++) {
        // printf("sort idx, val: %d, %f\n", (int)idx.at(i), error_now.at(idx.at(i)));

        bool add_point = true;
        for (const auto& output_pt : output_pts) {
            if (dist_between_points(pts1_now[idx.at(i)], output_pt) < params.min_pixel_dist) {
                add_point = false;
                break;
            }
        }

        if (add_point) {
            output_pts.push_back(pts1_now[idx.at(i)]);
            mask_min_pixel.at(idx.at(i)) = (uchar)1;
        }
    }

    //only keep points that passed min_pixel_test
    keep_masked(ids_now, mask_min_pixel);
    keep_masked(pts0_now, mask_min_pixel);
    keep_masked(pts1_now, mask_min_pixel);

    if (params.en_timing) timers[new_cam_id].min_pixel_end = _apps_time_monotonic_ns();
    if (params.en_debug) printf("n pts post min pixel: %d\n", (int)pts0_now.size());
    if (params.en_logging) vft_log.pts_post_min_pixel = pts0_now.size();

    //initialize the outputs and then fill in the good values, match_mask was initialized to zeros at the top
    new_points = last_points;

    for (size_t i = 0; i < ids_now.size(); i++) {
        match_mask[ids_now[i]] = (uchar)1;
        new_points[ids_now[i]] = pts1_now[i];
    }

    std::lock_guard<std::mutex> n_prev_lock(cache_mutex);
 
    return 0;
}

void FeatureTracker::_new_imu_data_handler(__attribute__((unused)) int ch, char* data, int bytes, void* context){
    FeatureTracker* ctx = (FeatureTracker*)context;

    int n_packets;
    imu_data_t* data_array = pipe_validate_imu_data_t(data, bytes, &n_packets);

    if (data_array == NULL) return;
    if (n_packets <= 0) return;

    for (int i = 0; i < n_packets; i++) {
        double gyro[3];
        double accel[3];

        gyro[0] = data_array[i].gyro_rad[0];
        gyro[1] = data_array[i].gyro_rad[1];
        gyro[2] = data_array[i].gyro_rad[2];

        accel[0] = data_array[i].accl_ms2[0];
        accel[1] = data_array[i].accl_ms2[1];
        accel[2] = data_array[i].accl_ms2[2];

        ImuBufferUnit curr_unit(data_array[i].timestamp_ns, accel, gyro);
        ctx->imu_ringbuf->insert_data(&curr_unit);
    }
}


int FeatureTracker::get_r_over_interval(int64_t ts_prev, int64_t ts_curr, size_t cam_id, cv::Mat& R_gyro){
    if (!params.en_gyro){
        return 0;
    }

    rc_matrix_t R = RC_MATRIX_INITIALIZER;
    rc_matrix_t tmp = RC_MATRIX_INITIALIZER;

    int64_t last_ts = 0;

    std::vector<ImuBufferUnit> units;
    imu_ringbuf->get_data_over_range(ts_prev, ts_curr, &units);

    rc_matrix_identity(&R, 3);

    for (size_t i = 0; i < units.size(); i++){
		if(last_ts==0){
			last_ts = units[i].timestamp_ns;
			continue;
		}

		double dt = ((double)(units[i].timestamp_ns-last_ts))/1000000000.0;
		last_ts = units[i].timestamp_ns;

		double dx = (double)units[i].w[0] * dt;
		double dy = (double)units[i].w[1] * dt;
		double dz = (double)units[i].w[2] * dt;

		rc_rotation_matrix_from_tait_bryan(dx, dy, dz, &tmp);
		rc_matrix_right_multiply_inplace(&R, tmp);
    }

    // now, correct the matrix to be in camera land!
    // put it in opencv matrix
    cv::Mat temp_R(3,3,CV_64F);
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            temp_R.at<double>(i,j) = R.d[i][j];
        }
    }

    // now multiply by cam to imu relation
    cv::Mat corrected_R = params.cams_wrt_imu[cam_id].inv() * temp_R * params.cams_wrt_imu[cam_id];

    // now just copy the R matrix into opencv land
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            R_gyro.at<double>(i,j) = corrected_R.at<double>(i, j);
        }
    }

    rc_matrix_free(&tmp);
    rc_matrix_free(&R);

    return 0;
}


int FeatureTracker::estimate_new_positions(std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &predicted_points, int64_t curr_ts, size_t cam_id){
    if (last_points.empty()){
        if (params.en_debug) M_ERROR("%s received empty points, %d\n", __FUNCTION__, __LINE__);
        return -1;
    }

    if (!params.en_gyro){
        predicted_points = last_points;
        return 0;
    }

    if (params.en_timing) timers[cam_id].estimation_start = _apps_time_monotonic_ns();

    int64_t last_timestamp_cp = last_timestamp_ns;

    cv::Mat accumulated_R = cv::Mat::eye(4, 4, CV_64F);

    int ret = get_r_over_interval(last_timestamp_cp, curr_ts, cam_id, accumulated_R);

    // now, do back prjection to bearing vectors
    std::vector<cv::Point3f> bearing_vecs;
    back_project_points(last_points, cam_id, bearing_vecs);

    // now rotate the points by accumulated gyro measurements
    std::vector<cv::Point3f> bearing_vecs_rotated;
    cv::perspectiveTransform(bearing_vecs, bearing_vecs_rotated, accumulated_R);

    // get back to image plane
    ret = forward_project_points(bearing_vecs_rotated, cam_id, predicted_points);

    if (params.en_timing) timers[cam_id].estimation_end = _apps_time_monotonic_ns();

    return ret;
}


int FeatureTracker::estimate_new_positions(std::vector<cv::Point2f> &last_points, std::vector<cv::Point2f> &predicted_points, size_t cam_id, cv::Mat &R){
    if (last_points.empty()){
        if (params.en_debug) M_ERROR("%s received empty points\n", __FUNCTION__);
        return -1;
    }

    if (params.en_timing) timers[cam_id].estimation_start = _apps_time_monotonic_ns();

    cv::Mat accumulated_R = cv::Mat::eye(4, 4, CV_64F);
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            accumulated_R.at<double>(i,j) = R.at<double>(i,j);
        }
    }

    // now, do back prjection to bearing vectors
    std::vector<cv::Point3f> bearing_vecs;
    int ret = back_project_points(last_points, cam_id, bearing_vecs);

    // now rotate the points by accumulated gyro measurements
    std::vector<cv::Point3f> bearing_vecs_rotated;
    cv::perspectiveTransform(bearing_vecs, bearing_vecs_rotated, accumulated_R);

    // get back to image plane
    ret = forward_project_points(bearing_vecs_rotated, cam_id, predicted_points);

    if (params.en_timing) timers[cam_id].estimation_end = _apps_time_monotonic_ns();

    return ret;
}



int FeatureTracker::back_project_points(std::vector<cv::Point2f> &points, size_t cam_id, std::vector<cv::Point3f> &projected_points){
    if (points.empty()){
        if (params.en_debug) M_ERROR("%s received empty points\n", __FUNCTION__);
        return -1;
    }

    std::vector<cv::Point2f> new_points;
    cv::undistortPoints(points, new_points, params.cams_mats[cam_id], params.cams_dists[cam_id]);

    for (size_t i = 0; i < new_points.size(); i++){
        cv::Point3f curr_bp(new_points[i].x, new_points[i].y, 1);
        projected_points.push_back(curr_bp);
    }

    return 0;
}


int FeatureTracker::forward_project_points(std::vector<cv::Point3f> &points, size_t cam_id, std::vector<cv::Point2f> &projected_points){
    if (points.empty()){
        if (params.en_debug)  M_ERROR("%s received empty points\n", __FUNCTION__);
        return -1;
    }

    cv::Mat rvec = cv::Mat::zeros(3,1,CV_64F);
    cv::Mat tvec = cv::Mat::zeros(3,1,CV_64F);

    cv::projectPoints(points, rvec, tvec, params.cams_mats[cam_id], params.cams_dists[cam_id], projected_points);

    return 0;
}


cv::Point2f FeatureTracker::undistort_point(const cv::Point2f &cv_point, size_t cam_id, bool is_fisheye)
{
    Eigen::Vector2f orig_pt;
    orig_pt << cv_point.x, cv_point.y;

    cv::Matx33d cam_matrix = params.cams_mats[cam_id];
    cv::Vec4d cam_distortion = params.cams_dists[cam_id];

    cv::Mat new_pt(1, 2, CV_32F);
    new_pt.at<float>(0, 0) = orig_pt(0);
    new_pt.at<float>(0, 1) = orig_pt(1);
    new_pt = new_pt.reshape(2);

    if (is_fisheye){
        cv::fisheye::undistortPoints(new_pt, new_pt, cam_matrix, cam_distortion);
    }
    else {
        cv::undistortPoints(new_pt, new_pt, cam_matrix, cam_distortion);
    }

    new_pt = new_pt.reshape(1);

    cv::Point2f pt_out;
    pt_out.x = new_pt.at<float>(0, 0);
    pt_out.y = new_pt.at<float>(0, 1);
    return pt_out;
}


int FeatureTracker::draw_overlay(cv::Mat &overlay)
{
    if(params.en_debug) printf("drawing overlay\n");
    cache_mutex.lock();

    std::map<size_t, std::vector<cv::Point2f>> prev_points;
    std::map<size_t, std::vector<cv::Point2f>> curr_points;
    std::map<size_t, std::vector<cv::Mat>> pyramids;
    std::map<size_t, std::vector<size_t>> ids;

    prev_points = prev_matched;
    curr_points = curr_matched;
    pyramids = last_pyramid;
    ids = curr_ids;

    cache_mutex.unlock();

    static int size = 2;
    if (params.width < 640) size = 1;

    for (size_t i = 0; i < curr_points.size(); i++){

        cv::Mat cached_prev = pyramids[i][0].clone();

        // check if image is correct dimensions, if not, resize
        if (cached_prev.rows != params.width || cached_prev.cols != params.height){
            cv::resize(cached_prev, cached_prev, cv::Size(params.width, params.height));
        }

        // lets expand our image to rgb for clarity
        // cv::Mat rgb_cache = cached_prev;

        cv::Mat rgb_cache;
        cv::Mat in[] = {cached_prev, cached_prev, cached_prev};
        cv::merge(in, 3, rgb_cache);


        //change how new and old points are plotted
        for (size_t j = 0; j < curr_points[i].size(); j++){
            // printf("pt id: %d\n", (int)curr_ids[i][j]);

            auto prev_id_it = std::find(prev_ids[i].begin(), prev_ids[i].end(), curr_ids[i][j]);

            if (prev_id_it != prev_ids[i].end()){
                // found current id in previous ids
                int prev_id_index = std::distance(prev_ids[i].begin(), prev_id_it);
                cv::drawMarker(rgb_cache, curr_points[i][j], cv::Scalar(0, 255, 0), cv::MARKER_SQUARE, 4 * size, size);
                cv::line(rgb_cache, prev_points[i][prev_id_index], curr_points[i][j], cv::Scalar(0, 0, 255), size);
            }
            else {
                //did not find it, may be a new point
                cv::drawMarker(rgb_cache, curr_points[i][j], cv::Scalar(255, 0, 0), cv::MARKER_SQUARE, 4 * size, size);
            }
        }

        if (i==0) overlay = rgb_cache;
        else cv::vconcat(overlay, rgb_cache, overlay);
    }

    // multiple images
    // if (prev_points.size() > 1) cv::resize(overlay, overlay, cv::Size(overlay.cols* 3/4, overlay.rows  * 3/4));

    return 0;
}


void FeatureTracker::reset_timer(size_t cam_id){
    memset(&timers[cam_id], 0, sizeof(timing_vars));
}

float FeatureTracker::dist_between_points(const cv::Point2f &pt0, const cv::Point2f &pt1) {
    float dx = pt0.x - pt1.x;
    float dy = pt0.y - pt1.y;
    return std::sqrt(dx*dx + dy*dy);
}

template <typename T> 
int FeatureTracker::keep_masked(std::vector<T> &v, const std::vector<uchar> &mask) {
    if (mask.size() != v.size()) {
        printf("FAIL: mask not the same size as vector in keep_maskd");
        return -1;
    }

    //TODO, improve this algorithm using iterators
    auto v_temp = v;
    v.clear();

    for (size_t i = 0; i < mask.size(); i++) {
        if (mask[i]) {
            v.push_back(v_temp[i]);
        }
    }
    return 0;
}


FeatureTracker::~FeatureTracker(){
    if (imu_ringbuf){
        delete imu_ringbuf;
    }

    // #ifdef BUILD_QRB5165
    // de-allocate feature buffers
    // if (!mcv_features.empty()){
    //     for (size_t i = 0; i < mcv_features.size(); i++){
    //         free(mcv_features[i]);
    //     }
    // }

    // #endif
}


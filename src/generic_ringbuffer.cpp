/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "generic_ringbuffer.h"

template <typename BufUnit>
int RingBuffer<BufUnit>::insert_data(BufUnit* new_packet){
    int new_index;

    // sanity checks
    if(unlikely(data==NULL)){
        fprintf(stderr,"ERROR in %s, received NULL pointer\n", __FUNCTION__);
        return -1;
    }

    // we are about to interact with the ringbuf, lock the mutex
    buf_mutex.lock();

    // more sanity checks
    if(unlikely(!initialized)){
        buf_mutex.unlock();
        fprintf(stderr,"ERROR in %s, ringbuf uninitialized\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(new_packet->timestamp_ns <= latest_timestamp_ns)){
        buf_mutex.unlock();
        fprintf(stderr,"ERROR in %s, detected timestamp out of order\n", __FUNCTION__);
        return -1;
    }

    // if this is the first thing to be entered make sure to start at zero
    if(items_in_buf==0){
        new_index = 0;
    }
    else{
        // increment index and check for loop-around
        new_index = index+1;
        if(new_index >= size) new_index = 0;
    }

    // copy the data into our buffer
    memcpy(&data[new_index], new_packet, sizeof(BufUnit));

    // bump index and increment number of items if necessary
    index = new_index;
    if(items_in_buf < size){
        items_in_buf++;
    }

    // all done, save the timestamp and unlock mutex
    latest_timestamp_ns = new_packet->timestamp_ns;
    buf_mutex.unlock();
    return 0;
}

template <typename BufUnit>
int RingBuffer<BufUnit>::get_data_at_position(int position, BufUnit* result){
    // sanity checks
    if(unlikely(data==NULL || result==NULL)){
        fprintf(stderr,"ERROR in %s, received NULL pointer\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(position<0)){
        fprintf(stderr,"ERROR in %s, position must be >= 0\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(!initialized)){
        fprintf(stderr,"ERROR in %s, ringbuf uninitialized\n", __FUNCTION__);
        return -1;
    }

    // about to start reading the buffer, lock the mutex
    buf_mutex.lock();

    // silently return if user requested a position beyond buffer size
    if(position >= size){
        buf_mutex.unlock();
        return -3;
    }
    // silently return if user requested an item that hasn't been added yet
    if(position >= items_in_buf){
        buf_mutex.unlock();
        return -2;
    }

    // return index is just latest index minus position due to the order we keep
    // data (populated from left to right)
    int return_index = index - position;

    // check for looparound
    if(return_index < 0){
        return_index += size;
    }

    // write out data
    *result = data[return_index];

    // all done, unlock mutex
    buf_mutex.unlock();

    return 0;
}


template <typename BufUnit>
BufUnit* RingBuffer<BufUnit>::get_data_ptr_at_position(int position){
    // return index is just latest index minus position due to the order we keep
    // data (populated from left to right)
    int return_index = index - position;

    // check for looparound
    if(return_index < 0){
        return_index += size;
    }

    // write out data
    return &data[return_index];
}


template <typename BufUnit>
int64_t RingBuffer<BufUnit>::get_timestamp_at_position(int position){
    // silently return if user requested an item that hasn't been added yet
    if(position >= items_in_buf){
        return -2;
    }

    // return index is just latest index minus position due to the order we keep
    // data (populated from left to right)
    int return_index = index - position;

    // check for looparound
    if(return_index < 0){
        return_index += size;
    }

    // return the requested timestamp
    return data[return_index].timestamp_ns;
}


template <typename BufUnit>
int RingBuffer<BufUnit>::get_data_at_time(int64_t timestamp_ns, BufUnit* result){
    // sanity checks
    if(unlikely(data==NULL)){
        fprintf(stderr,"ERROR in %s, received NULL pointer\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(!initialized)){
        fprintf(stderr,"ERROR in %s, ringbuf uninitialized\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(timestamp_ns<=0)){
        fprintf(stderr,"ERROR in %s, requested timestamp must be >0\n", __FUNCTION__);
        return -1;
    }
    if(items_in_buf < 2){
        fprintf(stderr,"ERROR in %s, not enough data in buffer!\n", __FUNCTION__);
        return -2;
    }

    // about to start messing with the buffer, lock the mutex
    buf_mutex.lock();

    // allow timestamps up to 0.2s newer than our last position record
    if(timestamp_ns > (latest_timestamp_ns+200000000)){
        fprintf(stderr,"ERROR in %s, timestamp too new\n", __FUNCTION__);
        buf_mutex.unlock();
        return -3;
    }
    // don't deal with timestamps older than our buffer has data for
    if(timestamp_ns < get_timestamp_at_position(items_in_buf-1)){
        fprintf(stderr, "ERROR in %s, requested timestamp older than oldest member in buffer\n", __FUNCTION__);
        buf_mutex.unlock();
        return -4;
    }

    // next logic is going to be to find the two transforms to interpolate over
    BufUnit *_ptr_before, *_ptr_after;
    
    // check for timestamp newer than we have record of, if so, extrapolate given
    // the two most recent records
    if(timestamp_ns > latest_timestamp_ns){
        _ptr_before = get_data_ptr_at_position(1);
        _ptr_after  = get_data_ptr_at_position(0);
    }

    // now go searching through the buffer to find which two entries to 
    // interpolate between, starting from newest. TODO: binary search
    else{
        for(int i=0;i<items_in_buf;i++){
            // timestamp to check at this point
            int64_t ts_at_i = get_timestamp_at_position(i);

            // found the right value! no interpolation needed
            if(ts_at_i == timestamp_ns){
                BufUnit* _data_ptr = get_data_ptr_at_position(i);
                memcpy(result, _data_ptr, sizeof(BufUnit));
                buf_mutex.unlock();
                return 0;
            }

            // once we get a timestamp older than requested ts, we have found the
            // right interval, grab the appropriate transforms
            if(ts_at_i < timestamp_ns){
                _ptr_before = get_data_ptr_at_position(i);
                _ptr_after  = get_data_ptr_at_position(i-1);
                break;
            }
        }
    }

    // calculate interpolation constant h which is between 0 and 1.
    // 0 would be right at _ptr_before, 1 would be right at _ptr_after.
    double h = (double)(timestamp_ns-_ptr_before->timestamp_ns) / (double)(_ptr_after->timestamp_ns-_ptr_before->timestamp_ns);

    // if no velocity, do linear interpolation
    // TODO check angular velocity too
    int ret = _ptr_before->interpolate(_ptr_before, _ptr_after, h, result);
    
    buf_mutex.unlock();
    return ret;
}


template <typename BufUnit>
int RingBuffer<BufUnit>::get_data_over_range(int64_t prev_ts_ns, int64_t curr_ts_ns, std::vector<BufUnit> *result_vec){
    // sanity checks
    if(unlikely(data==NULL)){
        fprintf(stderr,"ERROR in %s, received NULL pointer\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(!initialized)){
        fprintf(stderr,"ERROR in %s, ringbuf uninitialized\n", __FUNCTION__);
        return -1;
    }
    if(unlikely(curr_ts_ns<=0) || unlikely(prev_ts_ns<=0)){
        fprintf(stderr,"ERROR in %s, requested timestamp must be >0\n", __FUNCTION__);
        return -1;
    }
    if(items_in_buf < 2){
        fprintf(stderr,"ERROR in %s, not enough data in buffer!\n", __FUNCTION__);
        return -2;
    }

    // about to start messing with the buffer, lock the mutex
    buf_mutex.lock();

    // allow timestamps up to 0.2s newer than our last record
    if(curr_ts_ns > (latest_timestamp_ns+200000000)){
        fprintf(stderr,"ERROR in %s, timestamp too new\n", __FUNCTION__);
        buf_mutex.unlock();
        return -3;
    }

    // don't deal with timestamps older than our buffer has data for
    if(prev_ts_ns < get_timestamp_at_position(items_in_buf-1)){
        fprintf(stderr, "ERROR in %s, requested timestamp older than oldest member in buffer\n", __FUNCTION__);
        buf_mutex.unlock();
        return -4;
    }

    // next logic is going to be to find the two positions in our buffer that bound our timerange
    int position_start = -1;
    int position_end = -1;
    bool start_set = false;
    bool end_set = false;

    // check for timestamp newer than we have record of, if so, extrapolate given
    // the two most recent records
    if(curr_ts_ns > latest_timestamp_ns){
        position_end = 0;
        end_set = true;
    }


    for(int i=0;i<items_in_buf;i++){
        // timestamp to check at this point
        int64_t ts_at_i = get_timestamp_at_position(i);

        if (!start_set){
            if(ts_at_i == prev_ts_ns) {
                position_start = i;
                start_set = true;
            }
            else if (ts_at_i < prev_ts_ns){
                position_start = i;
                start_set = true;
            }
        }

        if (!end_set){
            if (ts_at_i == curr_ts_ns){
                position_end = i;
                end_set = true;
            }
            else if (ts_at_i < curr_ts_ns){
                position_end = i;
                end_set = true;
            }
        }

        if (end_set && start_set) break;
    }



    for(int i=position_end;i<position_start;i++){
        BufUnit curr_entry;
        curr_entry = *(get_data_ptr_at_position(i));
        result_vec->push_back(curr_entry);
    }

    buf_mutex.unlock();
    return 0;
}
/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/*
 * @file        main.cpp
 *
 * @brief       Driver code for running voxl-feature-tracker
 *
 *              There are two primary modes for running this feature tracker.
 *              The first is the OpenCV implementation which makes use of the
 *              aforementioned library to recognize feature points and track
 *              them. The second mode is the CVP implementation which uses
 *              logic defined in /core-libs/libmodal-cv to do the same task.
 *              This latter implementation offloads core logic to onboard
 *              discrete signal processors and is thus generally more
 *              computationally and thermally efficient. However the performance
 *              is marginally worse.
 *
 *              Configuration for this module can be found in
 *              /etc/modalai/voxl-feature-tracker.conf . In addition to choosing
 *              a feature tracker mode for each camera you can also choose a
 *              number of features to (ideally) track.
 *
 * @author      Thomas Patton (thomas.patton@modalai.com)
 * @date        2024
 */

#include <getopt.h>
#include <modal_journal.h>
#include <modal_pipe.h>
#include <modal_pipe_interfaces.h>
#include <modal_pipe_server.h>
#include <modalcv/common.h>
#include <opencv2/core/hal/interface.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <opencv2/features2d.hpp>
#include <thread>
#include <unordered_map>

#include "command_interface.h"
#include "config_file.h"
#include "cvp_feature_tracker.h"
#include "feature_tracker.h"
#include "vft_interface.h"

#define PROCESS_NAME "voxl-feature-tracker"
#define CAMERA_CH_START_OFFSET 1
#define OVERLAY_CH_OFFSET 16
#define FEATURE_PIPE_SUFFIX "_feats"
#define OVERLAY_PIPE_SUFFIX "_feat_overlay"
#define FEATURE_NAME "tracked_feats"
#define FEATURE_LOCATION MODAL_PIPE_DEFAULT_BASE_DIR FEATURE_NAME "/"
#define OVERLAY_NAME "feat_overlay"
#define OVERLAY_LOCATION MODAL_PIPE_DEFAULT_BASE_DIR OVERLAY_NAME "/"

#define DRAW_BONUS_ROWS_TOP 32

/**
 * Variables to describe the state of the module
 */
std::mutex state_mutex;
std::atomic<vft_state> overall_state;

/**
 * Configuration variables for specialized run modes
 */
static int en_debug;
static bool en_timing;
static bool en_config_only;

/**
 * This vector holds the most recent callback times for the tracker. Logic
 * inside _camera_helper_cb insures this vector doesn't grow above 500 in
 * length. This vector is then used to compute the mean callback time for
 * logging purposes
 */
std::vector<int64_t> callback_times;
static int64_t frame_counter[MAX_CAMERAS];

/**
 * These vectors hold the trackers with one tracker for each distinct camera
 * channel. So if a camera is publishing on channel i and is specified to be a
 * CVP tracker, then cvp_tracker_vec will have an initialized tracker at index
 * (i - CAMERA_CH_START_OFFSET). The only exception to this is when "both" is
 * specified as a tracking option, in which case both vectors will have a
 * tracker initialized at that index
 */
std::vector<FeatureTracker> opencv_tracker_vec(MAX_CAMERAS);
std::vector<CVPFeatureTracker> cvp_tracker_vec(MAX_CAMERAS);

/**
 * @brief Defines a publisher for a given camera group
 */
struct camera_group_publisher_t {
    // which camera group index this is / what channel to publish on
    int group_index;
    int publish_ch;

    // mtx and cond for r/w on this camera group
    std::mutex pub_mtx;
    std::condition_variable pub_cond;

    // feature packet / features for this group
    vft_feature_packet feature_packet;
    std::vector<std::vector<vft_feature>> feature_data;

    // timestamps for this group
    std::vector<int64_t> group_timestamps;

    // if this group is ready to publish
    bool group_should_publish;
    //
    // camera_group_publisher_t(int index, int ch)
    //     : group_index(index), publish_ch(ch), group_should_publish(false) {
    //     // Initialize other members as necessary
    // }

    // the last frame_id published for each frame (to avoid duplicate publishes)
    std::vector<int> has_new_data;
};

/**
 * Two unordered maps intended at mapping a camera's flat channel index. The
 * first maps this channel to which camera group it belongs to. The second mapps
 * the channel to which "camera index" it is which is just a number between
 * [0-2] for which part of the vft_feature_packet it writes its results to
 */
std::unordered_map<int, int> ch_to_group_i;
std::unordered_map<int, int> ch_to_camera_i;

/**
 * Structs for each camera group publisher
 */
std::vector<std::thread> publisher_threads;
std::vector<camera_group_publisher_t> camera_group_publishers(
    MAX_CAMERA_GROUPS);

/**
 * Determines if this camera group is ready to publish based on camera
 * timestamps
 */
void should_group_publish(int group_i, int64_t this_timestamp) {
    vft_feature_packet feature_packet =
        camera_group_publishers[group_i].feature_packet;
    int n_group_cams = feature_packet.n_cams;

    // validates all cameras actually have new data
    for (int i = 0; i < n_group_cams; i++) {
        if(camera_group_publishers[group_i].has_new_data[i] == 0) {
            camera_group_publishers[group_i].group_should_publish = false;
            return;
        }
    }

    // don't think we need to lock here b/c this function is already locked by
    // this mtx std::lock_guard<std::mutex>
    // lock(camera_group_publishers[group_i].pub_mtx);
    for (int i = 0; i < n_group_cams; i++) {
        int64_t diff = this_timestamp -
                       camera_group_publishers[group_i].group_timestamps[i];
        if (diff > 10000000000) {  // add and for cam shutdown
            camera_group_publishers[group_i].group_should_publish = false;
            return;
        }
    }
    camera_group_publishers[group_i].group_should_publish = true;
}

void publisher_thread_fn(camera_group_publisher_t &camera_group_publisher) {
    std::unique_lock<std::mutex> lock(camera_group_publisher.pub_mtx);
    while (overall_state == RUNNING) {
        camera_group_publisher.pub_cond.wait(lock, [&] {
            return camera_group_publisher.group_should_publish ||
                   overall_state == STOPPING;  // need to cover stopping case
        });

        // on shutdown, close out the thread gracefully
        if (overall_state == STOPPING) return;

        // set timestamp of publish, rely on 0 camera for now
        camera_group_publisher.feature_packet.timestamp_ns =
            camera_group_publisher.group_timestamps[0];

        // fact check this publishing logic!
        int n_buffers = camera_group_publisher.feature_packet.n_cams + 1;
        const void *buffers[n_buffers];
        size_t lengths[n_buffers];
        buffers[0] = &camera_group_publisher.feature_packet;
        lengths[0] = sizeof(vft_feature_packet);
        for (int i = 1; i < n_buffers; i++) {
            buffers[i] = camera_group_publisher.feature_data[i - 1].data();
            lengths[i] = sizeof(vft_feature) *
                         camera_group_publisher.feature_packet.num_feats[i - 1];
        }
        if (en_debug) printf("Attempting to publish list to pipe...\n");
        pipe_server_write_list(camera_group_publisher.publish_ch, n_buffers,
                               buffers, lengths);

        for (int i = 0; i < camera_group_publisher.feature_packet.n_cams; i++) {
            camera_group_publisher.feature_data[i].clear();
            camera_group_publisher.has_new_data[i] = 0;
        }
        camera_group_publisher.group_should_publish = false;
    }
}

/**
 * @brief Print usage for voxl-feature-tracker
 */
static void _print_usage(void) {
    printf(
        "\n\
This is meant to run in the background as a systemd service, but can be\n\
run manually with the following debug options\n\
\n\
-c, --config                only parse the config file and exit, don't run\n\
-d, --debug                 enable debug prints\n\
-t, --timing                enable timing stats dump to console if debug is enabled\n\
-h, --help                  print this help message\n\
\n");
    return;
}

/**
 * @brief Parse user provided configuration options
 */
static bool _parse_opts(int argc, char *argv[]) {
    static struct option long_options[] = {{"config", no_argument, 0, 'c'},
                                           {"debug", no_argument, 0, 'd'},
                                           {"timing", no_argument, 0, 't'},
                                           {"help", no_argument, 0, 'h'},
                                           {0, 0, 0, 0}};

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "cdht", long_options, &option_index);

        // Detect the end of the options.
        if (c == -1) {
            break;
        }

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;

            case 'c':
                en_config_only = 1;
                break;

            case 'd':
                printf("Enabling debug mode\n");
                en_debug = 1;
                mcv_en_debug_prints(1);
                break;

            case 't':
                en_timing = 1;
                mcv_en_timing_prints(1);
                break;

            case 'h':
                _print_usage();
                return true;

            default:
                _print_usage();
                return true;
        }
    }
    return false;
}

/**
 * @brief Exits the program cleanly
 */
static void _quit(int ret) {
    if (en_debug) printf("Attempting to quit...\n");
    overall_state = STOPPING;

    // Close all the open pipe connections
    pipe_server_close_all();
    pipe_client_close_all();

    if (en_debug) printf("Closed server and client\n");

    // need to signal waiting cond vars so they can return
    for (size_t i = 0; i < camera_group_publishers.size(); i++) {
        camera_group_publishers[i].pub_cond.notify_one();
    }

    for (auto &thread : publisher_threads) {
        thread.join();
    }
    if (en_debug) printf("Closed threads\n");

    // remove this process ID file
    remove_pid_file(PROCESS_NAME);
    if (en_debug) printf("removed pid file\n");

    if (ret == 0)
        printf("Exiting Cleanly\n");
    else
        printf("error code %d\n", ret);
    exit(ret);
    return;
}

static void _camera_disconnect_cb(__attribute__((unused)) int ch,
                                  __attribute__((unused)) void *context) {
    fprintf(stderr, "Disonnected from camera pipe\n");
}

static void _feature_connect_cb(__attribute__((unused)) int ch,
                                __attribute__((unused)) int client_id,
                                __attribute__((unused)) char *name,
                                __attribute__((unused)) void *context) {
    printf("Client connected to feature tracker\n");
}

/**
 * @brief Primary callback for ingestion of camera frames
 *
 * @param ch The pipe channel the camera frame was ingested on
 * @param meta Metadata for the camera
 */
static void _camera_helper_cb(int ch, camera_image_metadata_t meta, char *frame,
                              void *context) {
    auto start = std::chrono::high_resolution_clock::now();

    state_mutex.lock();
    if (overall_state != RUNNING) {
        state_mutex.unlock();
        if (en_debug) printf("not running yet, exiting cam helper cb\n");
        return;
    }
    state_mutex.unlock();

    static int frame_id = 0;

    // KLT tracker output
    int num_features_to_track =
        tracker_input_vec[ch - CAMERA_CH_START_OFFSET].num_features;
    // static std::vector<vft_feature> feats_out(num_features_to_track);

    int camera_index = ch - CAMERA_CH_START_OFFSET;

    image_data curr_packet;
    curr_packet.timestamp_ns = meta.timestamp_ns;
    curr_packet.tracker_ids.push_back(camera_index);

    if (meta.format == IMAGE_FORMAT_RAW8) {
        // Unpack the data into an opencv image Mat
        cv::Mat img(meta.height, meta.width, CV_8UC1, frame);
        cv::blur(img, img, cv::Size(3, 3));
        // Create a mask for the ingestion.  We want the full image to be
        // ingested
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(mask);
    } else if (meta.format == IMAGE_FORMAT_RAW16) {
        cv::Mat img(meta.height, meta.width, CV_16UC1, frame);
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, frame);

        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(img);
    } else if (meta.format == IMAGE_FORMAT_RAW16) {
        // Unpack the data into an opencv image Mat
        cv::Mat img(meta.height, meta.width, CV_16UC1, frame);
        // Create a mask for the ingestion.  We want the full image to be
        // ingested
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(mask);
    } else if (meta.format == IMAGE_FORMAT_NV12 ||
               meta.format == IMAGE_FORMAT_NV21) {
        // Unpack the data into an opencv image Mat
        cv::Mat img(meta.height, meta.width, CV_8UC1, frame);
        cv::blur(img, img, cv::Size(3, 3));
        // Create a mask for the ingestion.  We want the full image to be
        // ingested
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(mask);

        meta.size_bytes = meta.width * meta.height;
        meta.format = IMAGE_FORMAT_RAW8;
    } else if (meta.format == IMAGE_FORMAT_STEREO_NV12 ||
               meta.format == IMAGE_FORMAT_STEREO_NV21) {
        // Unpack the data into opencv image Mats
        cv::Mat img(meta.height, meta.width, CV_8UC1, frame);
        cv::Mat img2(meta.height, meta.width, CV_8UC1,
                     frame + (meta.width * meta.height * 3 / 2));

        // testing lpf cv blur for tracking
        cv::blur(img, img, cv::Size(3, 3));
        cv::blur(img2, img2, cv::Size(3, 3));

        // Create masks for the ingestion. We want both full images to be
        // ingested
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));
        static cv::Mat mask2(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

        curr_packet.tracker_ids.push_back(camera_index + 1);
        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(mask);
        curr_packet.images.push_back(img2);
        curr_packet.masks.push_back(mask2);
    } else if (meta.format == IMAGE_FORMAT_STEREO_RAW8) {
        // Unpack the data into opencv image Mats
        cv::Mat img(meta.height, meta.width, CV_8UC1, frame);
        cv::Mat img2(meta.height, meta.width, CV_8UC1,
                     frame + (meta.width * meta.height));

        // testing lpf cv blur for tracking
        cv::blur(img, img, cv::Size(3, 3));
        cv::blur(img2, img2, cv::Size(3, 3));

        // Create masks for the ingestion. We want both full images to be
        // ingested
        static cv::Mat mask(meta.height, meta.width, CV_8UC1, cv::Scalar(0));
        static cv::Mat mask2(meta.height, meta.width, CV_8UC1, cv::Scalar(0));

        curr_packet.tracker_ids.push_back(camera_index + 1);
        curr_packet.images.push_back(img);
        curr_packet.masks.push_back(mask);
        curr_packet.images.push_back(img2);
        curr_packet.masks.push_back(mask2);
    }

    // get group/channel index, will be used soon
    int group_i = ch_to_group_i[ch];
    int camera_i = ch_to_camera_i[ch];

    // run the appropriate track function(s)
    int response;
    cv::Mat combined_overlay;
    if (tracker_input_vec[camera_index].tracker_type == TRACKER_OCV) {
        opencv_tracker_vec[camera_index].track_packet(
            curr_packet,
            camera_group_publishers[group_i].feature_data[camera_i]);
    } else if (tracker_input_vec[camera_index].tracker_type == TRACKER_CVP) {
        cvp_tracker_vec[camera_index].track(
            curr_packet,
            camera_group_publishers[group_i].feature_data[camera_i],
            num_features_to_track);
    } else {
        std::vector<vft_feature>
            dummy_output;  // so that ocv tracker doesn't write to actual
        opencv_tracker_vec[camera_index].track_packet(
            curr_packet,
            camera_group_publishers[group_i].feature_data[camera_i]);
        cvp_tracker_vec[camera_index].track(
            curr_packet,
            camera_group_publishers[group_i].feature_data[camera_i],
            num_features_to_track);
    }

    // if overlay is needed, draw it. note this is (w x (hx2)) when we're
    // tracking with TRACKER_BOTH
    if (pipe_server_get_num_clients(ch + OVERLAY_CH_OFFSET) > 0) {
        cv::Mat overl;
        if (tracker_input_vec[camera_index].tracker_type == TRACKER_OCV) {
            opencv_tracker_vec[camera_index].draw_overlay(overl);
        } else if (tracker_input_vec[camera_index].tracker_type ==
                   TRACKER_CVP) {
            cvp_tracker_vec[camera_index].draw_overlay(
                curr_packet,
                camera_group_publishers[group_i].feature_data[camera_i], overl);
        } else {
            cv::Mat cvp_overlay, ocv_overlay;
            opencv_tracker_vec[camera_index].draw_overlay(ocv_overlay);
            cvp_tracker_vec[camera_index].draw_overlay(
                curr_packet,
                camera_group_publishers[group_i].feature_data[camera_i],
                cvp_overlay);
            cv::hconcat(ocv_overlay, cvp_overlay, overl);
        }

        if (!overl.empty()) {
            camera_image_metadata_t draw_meta = meta;
            draw_meta.format = IMAGE_FORMAT_RGB;
            draw_meta.width = overl.cols;
            draw_meta.height = overl.rows;
            draw_meta.size_bytes = draw_meta.width * draw_meta.height * 3;
            draw_meta.timestamp_ns = meta.timestamp_ns;
            draw_meta.frame_id = frame_id++;
            pipe_server_write_camera_frame(ch + OVERLAY_CH_OFFSET, draw_meta,
                                           (char *)overl.data);
        }
    } else {
        if (en_debug) printf("no clients for overlay\n");
    }

    // pass along number of features to the packet + magic num
    camera_group_publishers[group_i].feature_packet.num_feats[camera_i] =
        (int)camera_group_publishers[group_i].feature_data[camera_i].size();
    camera_group_publishers[group_i].feature_packet.magic_number =
        VOXL_FT_MAGIC_NUMBER;

    // lock current r/w for camera group then write data
    {
        std::unique_lock<std::mutex> lock(
            camera_group_publishers[group_i].pub_mtx);
        camera_group_publishers[group_i].feature_packet.magic_number =
            VOXL_FT_MAGIC_NUMBER;
        camera_group_publishers[group_i].feature_packet.frame_ids[camera_i] =
            frame_counter[ch];
        camera_group_publishers[group_i].has_new_data[camera_i] = 1;
 
        // add current timestamp, then check if we should publish
        // per james, should be timestamp + 1/2 exposure
        int64_t offset_time = meta.timestamp_ns + meta.exposure_ns / 2;
        camera_group_publishers[group_i].group_timestamps[camera_i] =
            offset_time;
        should_group_publish(group_i, offset_time);

        if (frame_counter[ch] != 0)
            camera_group_publishers[group_i].pub_cond.notify_one();
    }

    // incr the frame counter for this camera
    frame_counter[ch] += 1;

    if (en_timing) {
        // timing stuff
        auto end = std::chrono::high_resolution_clock::now();
        auto duration =
            std::chrono::duration_cast<std::chrono::microseconds>(end - start)
                .count();
        if (callback_times.size() > 500) {
            callback_times.erase(callback_times.begin());
        }
        callback_times.push_back(duration);

        int time_sum = 0;
        for (const auto &time : callback_times) {
            time_sum += time;
        }

        // Convert microseconds to milliseconds and calculate the running
        // average
        double mean_time_ms =
            static_cast<double>(time_sum) / callback_times.size() / 1000.0;

        // Print the mean time in milliseconds with two decimal places
        std::cout << std::fixed << std::setprecision(2)
                  << "Mean Average Callback Time :: " << mean_time_ms << " ms"
                  << std::endl;
    }
}

// for qrb5165 only (right now) set the camera processing thread to run on
// CPU 7, big boy
static void _check_and_set_affinity(void) {
    // only do this once
    static int has_set = 0;
    if (has_set) return;

    cpu_set_t cpuset;
    pthread_t thread;
    thread = pthread_self();

    /* Set affinity mask to include CPUs 7 only */
    CPU_ZERO(&cpuset);
    // CPU_SET(0, &cpuset);
    // CPU_SET(1, &cpuset);
    // CPU_SET(2, &cpuset);
    // CPU_SET(3, &cpuset);
    CPU_SET(4, &cpuset);
    CPU_SET(5, &cpuset);
    CPU_SET(6, &cpuset);
    // CPU_SET(7, &cpuset); // only big core

    if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset)) {
        perror("pthread_setaffinity_np");
    }

    /* Check the actual affinity mask assigned to the thread */
    if (pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset)) {
        perror("pthread_getaffinity_np");
    }
    printf("Camera processing thread is now locked to the following cores:");
    for (int j = 0; j < CPU_SETSIZE; j++) {
        if (CPU_ISSET(j, &cpuset)) printf(" %d", j);
    }
    printf("\n");

    // only do this once on start
    has_set = 1;

    return;
}

static void _control_pipe_cb(__attribute__((unused)) int ch, char *string,
                             int bytes, __attribute__((unused)) void *context) {
    // remove the trailing newline from echo
    if (bytes > 1 && string[bytes - 1] == '\n') {
        string[bytes - 1] = 0;
    }
    if (strncmp(string, VFT_CMD_START, sizeof(VFT_CMD_START)) == 0) {
        printf("Client requested START\n");
        state_mutex.lock();
        overall_state = RUNNING;
        state_mutex.unlock();
        return;
    } else if (strncmp(string, VFT_CMD_PAUSE, sizeof(VFT_CMD_PAUSE)) == 0) {
        printf("Client requested PAUSE\n");
        state_mutex.lock();
        overall_state = PAUSED;
        state_mutex.unlock();
        return;
    } else {
        printf(
            "WARNING: Server received unknown command through the control "
            "pipe!\n");
        printf("got %d bytes. Command is: %s\n", bytes, string);
    }
    return;
}

static void _camera_connect_cb(__attribute__((unused)) int ch,
                               __attribute__((unused)) void *context) {
    printf("Got connection on channel %d\n", ch);
    int flags = SERVER_FLAG_EN_CONTROL_PIPE;
    int index = ch - CAMERA_CH_START_OFFSET;

    // get the pipe info associated with this channel
    pipe_info_t pipe_info;
    pipe_client_get_info(ch, &pipe_info);

    // grab some camera information
    int width, height, int_format, is_lepton;
    char format[CHAR_BUF_SIZE];
    cJSON *info = pipe_get_info_json(pipe_info.name);
    json_fetch_int_with_default(info, "width", &width, 1280);
    json_fetch_int_with_default(info, "height", &height, 800);
    json_fetch_int_with_default(info, "int_format", &int_format, 0);
    json_fetch_string(info, "string_format", format, CHAR_BUF_SIZE);
    is_lepton = !strncmp(pipe_info.name, "lepton0_16raw", CHAR_BUF_SIZE);

    // create the corresponding tracker(s)
    _check_and_set_affinity();
    if (tracker_input_vec[index].tracker_type == TRACKER_OCV ||
        tracker_input_vec[index].tracker_type == TRACKER_BOTH) {
        opencv_tracker_vec[index].init(
            width, height, tracker_input_vec[index].num_features, is_lepton);
    }
    if (tracker_input_vec[index].tracker_type == TRACKER_CVP ||
        tracker_input_vec[index].tracker_type == TRACKER_BOTH) {
        cvp_tracker_vec[index].init(width, height, int_format == 2);
    }

    // configure publisher logic
    int i = ch - CAMERA_CH_START_OFFSET;
    int group_index = tracker_input_vec[i].group_index;
    ch_to_group_i[ch] = group_index;
    ch_to_camera_i[ch] = tracker_input_vec[i].camera_index;
    printf("Binding channel %d to group=%d cam=%d\n", ch, ch_to_group_i[ch],
           ch_to_camera_i[ch]);
    camera_group_publishers[group_index].feature_packet.n_cams += 1;

    // only continue to set up pipe stuff (and some other) if first invoke per group
    if (camera_group_publishers[group_index].feature_packet.n_cams > 1) return;

    // only want to do this once per group
    for (int i = 0; i < MAX_CAMERAS_PER_GROUP; i++) {
        camera_group_publishers[group_index].has_new_data.push_back(1);
        camera_group_publishers[group_index].group_timestamps.push_back(0);
        camera_group_publishers[group_index].feature_data.push_back(
            std::vector<vft_feature>());
    }
    camera_group_publishers[group_index].group_should_publish = false;
    camera_group_publishers[group_index].publish_ch = ch;

    // logic for feature output pipe
    pipe_info_t feature_info;
    feature_info.size_bytes = 1280 * 800 * 64;
    feature_info.server_pid = 0;

    // char logic to pass name
    strcpy(feature_info.name, tracker_input_vec[index].output_pipe);
    feature_info.name[MODAL_PIPE_MAX_NAME_LEN - 1] = '\0';

    // char logic to get location
    char feature_location[MODAL_PIPE_MAX_DIR_LEN];
    strncpy(feature_location, MODAL_PIPE_DEFAULT_BASE_DIR,
            MODAL_PIPE_MAX_DIR_LEN);
    strncat(feature_location, tracker_input_vec[index].output_pipe,
            MODAL_PIPE_MAX_DIR_LEN - strlen(MODAL_PIPE_DEFAULT_BASE_DIR));
    strncpy(feature_info.location, feature_location, MODAL_PIPE_MAX_DIR_LEN);
    feature_info.location[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0';

    // char logic to pass type
    strncpy(feature_info.type, "vft_feature_packet", MODAL_PIPE_MAX_TYPE_LEN);
    feature_info.type[MODAL_PIPE_MAX_TYPE_LEN - 1] = '\0';

    // create pipe
    if (pipe_server_create(ch, feature_info, flags)) {
        fprintf(stderr, "ERROR creating feature pipe\n");
    }

    pipe_server_set_control_cb(ch, _control_pipe_cb, NULL);
    pipe_server_set_connect_cb(ch, _feature_connect_cb, NULL);
    pipe_server_set_available_control_commands(ch, CONTROL_COMMANDS);

    // logic for feature overlay pipe
    pipe_info_t overlay_info;
    overlay_info.size_bytes = 1280 * 800 * 64;
    overlay_info.server_pid = 0;

    // char logic to pass name
    strncpy(overlay_info.name, tracker_input_vec[index].overlay_pipe,
            MODAL_PIPE_MAX_NAME_LEN);
    overlay_info.name[MODAL_PIPE_MAX_NAME_LEN - 1] = '\0';

    // char logic to get location
    char overlay_location[MODAL_PIPE_MAX_DIR_LEN];
    strncpy(overlay_location, MODAL_PIPE_DEFAULT_BASE_DIR,
            MODAL_PIPE_MAX_DIR_LEN);
    strncat(overlay_location, tracker_input_vec[index].overlay_pipe,
            MODAL_PIPE_MAX_DIR_LEN - strlen(MODAL_PIPE_DEFAULT_BASE_DIR));
    strncpy(overlay_info.location, overlay_location, MODAL_PIPE_MAX_DIR_LEN);
    overlay_info.location[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0';

    // char logic to pass type
    strncpy(overlay_info.type, "camera_image_metadata_t",
            MODAL_PIPE_MAX_TYPE_LEN);
    overlay_info.type[MODAL_PIPE_MAX_TYPE_LEN - 1] = '\0';

    // create pipe
    if (pipe_server_create(ch + OVERLAY_CH_OFFSET, overlay_info, flags)) {
        fprintf(stderr, "ERROR creating feature pipe\n");
    }

    printf("=========================================\n");
    printf("Connected to %s\n", pipe_info.name);
    printf("Image resolution: [%d x %d]\n", width, height);
    printf("Image format: %s\n", format);
    printf("N Features: %d\n", tracker_input_vec[index].num_features);
    printf("Tracker Type: %d\n", tracker_input_vec[index].tracker_type);
    printf("Feature Pipe: %s\n", feature_info.location);
    printf("Overlay Pipe: %s\n", overlay_info.location);
    printf("=========================================\n");

    // invoke publisher thread for this group
    publisher_threads.emplace_back(
        publisher_thread_fn, std::ref(camera_group_publishers[group_index]));
}

static int _connect_client_pipes(void) {
    int actual_index = 0;

    // for each input, attempt to connect
    for (size_t i = 0; i < tracker_input_vec.size(); i++) {
        // configure group/cam indices for channel
        int channel_number = CAMERA_CH_START_OFFSET + actual_index;

        pipe_client_set_disconnect_cb(channel_number, _camera_disconnect_cb,
                                      NULL);
        pipe_client_set_connect_cb(channel_number, _camera_connect_cb, NULL);
        pipe_client_set_camera_helper_cb(channel_number, _camera_helper_cb,
                                         NULL);
        int flags = CLIENT_FLAG_EN_CAMERA_HELPER;
        printf("Attempting to open %s pipe as client...\n",
               tracker_input_vec[i].input_pipe);
        if (pipe_client_open(channel_number, tracker_input_vec[i].input_pipe,
                             PROCESS_NAME, flags, 0) != 0) {
            M_ERROR("FAILED TO OPEN %s\n", tracker_input_vec[i].input_pipe);
            return -1;
        }
        actual_index += 1;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    // Parse the command line options and terminate if the parser says we should
    // terminate
    if (_parse_opts(argc, argv)) {
        return -1;
    }

    /* make sure another instance isn't running
     * if return value is -3 then a background process is running with
     * higher privaledges and we couldn't kill it, in which case we should
     * not continue or there may be hardware conflicts. If it returned -4
     * then there was an invalid argument that needs to be fixed.
     */
    if (kill_existing_process(PROCESS_NAME, 2.0) < -2) {
        fprintf(stderr, "ERROR: could not kill existing process\n");
        _quit(-1);
    }

    // start signal handler so we can exit cleanly
    if (enable_signal_handler() == -1) {
        fprintf(stderr, "ERROR: failed to start signal handler\n");
        _quit(-1);
    }

    // Set the main running flag to 1 to indicate that we are running
    main_running = 1;

    cv::setNumThreads(1);

    /* make PID file to indicate your project is running
     * due to the check made on the call to rc_kill_existing_process() above
     * we can be fairly confident there is no PID file already and we can
     * make our own safely.
     */
    make_pid_file(PROCESS_NAME);

    // Load the config files
    printf("Loading our own config file\n");
    if (config_file_read() < 0) {
        fprintf(stderr, "ERROR %d\n", config_file_read());
        _quit(-1);
    }

    if (en_config_only) _quit(0);

    // Connect to the client pipes and start getting data
    printf("Creating client pipes...\n");
    if (_connect_client_pipes() < 0) _quit(0);

    // run until start/stop module catches a signal and changes main_running to
    // 0
    printf("Setup completed! Waiting for images...\n");
    while (main_running) usleep(5000000);

    // Shutdown Nicely
    _quit(0);

    return 0;
}

/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/*
 * @file        voxl-inspect-features.cpp
 *
 * @brief       Creates a simple pipe client to read out the status of
 *              voxl-feature-tracker
 *
 * @author      Thomas Patton (thomas.patton@modalai.com)
 * @date        2024
 */

#include <getopt.h>
#include <modal_pipe.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  // for usleep()
#include <vft_interface.h>

const char* CLIENT_NAME = "voxl-inspect-features";
const char* IN_PIPE_NAME = "tracking_feats";

// #define GPS_RAW_OUT_PATH	(MODAL_PIPE_DEFAULT_BASE_DIR
// "features_raw_int/")

const char* CLEAR_TERMINAL = "\033c";        // same as typing "clear" in bash
const char* CLEAR_SCREEN = "\033[H\033[2J";  // like "clear -x" in bash
const char* DISABLE_WRAP =
    "\033[?7l";  // disables line wrap, be sure to enable before exiting
const char* ENABLE_WRAP = "\033[?7h";  // default terminal behavior
const char* CLEAR_LINE = "\033[2K";    // erases line but leaves curser in place
const char* GOTO_TOP_LEFT = "\033[f";  // move curser to top left
const char* RESET_FONT = "\x1b[0m";    // undo any font/color settings
const char* FONT_BOLD = "\033[1m";     // bold font
const char* FONT_LIGHT = "\033[2m";    // lighter font color
const char* FONT_UNDERLINE = "\033[4m";  // underline
const char* FONT_BLINK =
    "\033[5m";  // text keeps blinking even after program exits
const char* FONT_INVERSE = "\033[7m";    // background/foreground inverted
const char* FONT_INVISIBLE = "\033[8m";  // text color same as background
const char* COLOR_RED = "\033[31m";      // red
const char* COLOR_GRN = "\033[32m";      // green
const char* COLOR_YLW = "\033[33m";      // yellow
const char* COLOR_BLU = "\033[34m";      // blue
const char* COLOR_MAG = "\033[35m";      // magenta
const char* COLOR_CYN = "\033[36m";      // cyan
const char* COLOR_WHT = "\033[37m";      // white

static int newline = 0;

// structs for inspection
vft_feature_packet* feature_packet;
vft_feature* features;

static void _print_usage(void) {
    printf(
        "\n\
Tool to print data being delivered to the feature tracker pipe\n\
\n\
-h, --help              print this help message\n\
\n\
\n");
    return;
}

static int parse_opts(int argc, char* argv[]) {
    static struct option long_options[] = {{"help", no_argument, 0, 'h'},
                                           {0, 0, 0, 0}};

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "h", long_options, &option_index);

        if (c == -1) break;  // Detect the end of the options.

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;
            case 'h':
                _print_usage();
                exit(0);
            default:
                _print_usage();
                return -1;
        }
    }

    return 0;
}

// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch,
                        __attribute__((unused)) void* context) {
    // get some space for the features
    create_vft_memory(&feature_packet, &features);

    printf("%s%s%s", CLEAR_TERMINAL, DISABLE_WRAP, FONT_BOLD);
    printf("     Timestamp    |  Camera #  | Num Features  \n");
    printf("+-----------------+------------+--------------+\n");
    printf("%s", RESET_FONT);
    return;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch,
                           __attribute__((unused)) void* context) {
    // destroy feature memory
    destroy_vft_memory(&feature_packet, &features);
    fprintf(stderr, "\r %s %s %s %s", CLEAR_LINE, FONT_BLINK,
            "feature tracker disconnected", RESET_FONT);
    return;
}

// ///////////////////////////////////////////////////////
// ONLY FOR DEBUG CHECK!
// Delete when production
// ///////////////////////////////////////////////////////
static void _debug_helper_cb(__attribute__((unused)) int ch, char* data, int bytes,
                       __attribute__((unused)) void* context) {
    // use validation function to extract out packet/feature data from pipe
    if (validate_vft_data(ch, data, bytes, &feature_packet, &features)) {
        printf("ERROR parsing vft data from pipe...\n");
        return;
    }
    double set_time = (double) feature_packet->timestamp_ns * 1e-9;

    static double last_set_time = set_time;

    double dt_f = set_time-last_set_time;

    last_set_time = set_time;

    printf("t: %f (dt: %f)\n", set_time, dt_f);
    printf("%d %d\n", feature_packet->frame_ids[0], feature_packet->frame_ids[1]);
    printf("---\n");

    return;
}

static void _helper_cb(__attribute__((unused)) int ch, char* data, int bytes,
                       __attribute__((unused)) void* context) {
    // use validation function to extract out packet/feature data from pipe
    if (validate_vft_data(ch, data, bytes, &feature_packet, &features)) {
        printf("ERROR parsing vft data from pipe...\n");
        return;
    }
    int n_cams = feature_packet->n_cams;
    int n_total_features = 0;
    for (int i = 0; i < n_cams; i++) {
        n_total_features += feature_packet->num_feats[i];
    }

    // TODO: maybe add some logic to check that the features come out right :)
    int sum_age[n_cams];
    int current_cam = 0;
    int relative_counter = 0;
    for (int i = 0; i < n_total_features; i++) {
        if (relative_counter > feature_packet->num_feats[current_cam]) {
            current_cam++;
            relative_counter = 0;
        }
        sum_age[current_cam] += features[i].age;
        relative_counter++;
    }

    // print out info on the incoming features
    printf("%s%s%s", CLEAR_TERMINAL, DISABLE_WRAP, FONT_BOLD);
    printf(
        "+-----------------+------------+--------------+------------------+\n");
    printf(
        "|    Timestamp    |  Camera #  | Num Features | Mean Feature Age |\n");
    printf(
        "+-----------------+------------+--------------+------------------+\n");
    printf("%s", RESET_FONT);
    for (int i = 0; i < feature_packet->n_cams; i++) {
        printf("|  %ld  |   %3d      |     %3d      |     %7d      |\n",
               feature_packet->timestamp_ns, i, feature_packet->num_feats[i],
               sum_age[i] / feature_packet->num_feats[i]);
    }
    printf(
        "+-----------------+------------+--------------+------------------+\n");
    fflush(stdout);
    return;
}

int main(int argc, char* argv[]) {
    int ret = 0;

    // check for options
    if (parse_opts(argc, argv)) return -1;

    // set some basic signal handling for safe shutdown.
    // quitting without cleanup up the pipe can result in the pipe staying
    // open and overflowing, so always cleanup properly!!!
    enable_signal_handler();
    main_running = 1;

    // normal non-test mode
    // set up all our MPA callbacks
    pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
    pipe_client_set_connect_cb(0, _connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

    ret = pipe_client_open(0, IN_PIPE_NAME, CLIENT_NAME,
                           EN_PIPE_CLIENT_SIMPLE_HELPER,
                           sizeof(vft_feature_packet));
    if (ret) {
        fprintf(stderr, "ERROR: failed to open pipe %s\n", IN_PIPE_NAME);
        pipe_print_error(ret);
        fprintf(stderr, "Probably voxl-feature-tracker is not running\n");
        return ret;
    }

    // check for errors trying to connect to the server pipe
    if (ret < 0) {
        pipe_print_error(ret);
        printf("%s", ENABLE_WRAP);
        return -1;
    }

    // keep going until the  signal handler sets the running flag to 0
    while (main_running) usleep(500000);

    // all done, signal pipe read threads to stop
    printf("\nclosing and exiting...\n %s %s", RESET_FONT, ENABLE_WRAP);
    pipe_client_close_all();

    return 0;
}
